package com.covid2019.rchc.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;
import android.view.WindowManager;

import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;

public class Splash extends AppCompatActivity {

    private UserSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        session = new UserSession(Splash.this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
//            Window window = getWindow();

            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));


        }

        new Handler().postDelayed(new Runnable() {


            @Override
            public void run() {
                // This method will be executed once the timer is over
                Intent intent = new Intent(Splash.this, Intro.class);
                startActivity(intent);
                finish();
            }
        }, 2000);

    }

    @Override
    protected void onStart() {
        super.onStart();

       /* if (session.isLangChange()){
            Intent intent = new Intent(Splash.this, MainActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);

            session.setIslanchange(false);
            finish();

        } else {
            new Handler().postDelayed(new Runnable() {


                @Override
                public void run() {
                    // This method will be executed once the timer is over
                    Intent intent = new Intent(Splash.this, Intro.class);
                    startActivity(intent);
                    finish();
                }
            }, 2000);
        }*/
    }
}