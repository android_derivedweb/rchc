package com.covid2019.rchc.Fragments;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Adapter.ExpListAdapterFrequentAsked;
import com.covid2019.rchc.Adapter.IssueViewAdapter;
import com.covid2019.rchc.Api.GetIssues;
import com.covid2019.rchc.Model.IssueChildModel;
import com.covid2019.rchc.Model.IssueGroupModel;
import com.covid2019.rchc.Model.IssueModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Issues extends Fragment {

  //  private RecyclerView mIssues;
  //  private IssuesAdapter mIssuesAdapter;
    private UserSession session;

    private ExpListAdapterFrequentAsked mExapandableAdapter;

    private ArrayList<IssueModel> issueModelArrayList = new ArrayList<>();

    private ArrayList<IssueGroupModel> issueGroupModelArrayList = new ArrayList<>();

    private ExpandableListView expandableListView;

    private LinearLayout dwdsd;
    private IssueViewAdapter issueViewAdapter;
    private RequestQueue requestQueue;
    private String search = "";


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_issues, container, false);

        session = new UserSession(getContext());

        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        ViewPager mViewPager = (ViewPager) view.findViewById(R.id.viewPager);

        expandableListView = view.findViewById(R.id.expandableListView);
        dwdsd = view.findViewById(R.id.dwdsd);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

      /*  mIssues = view.findViewById(R.id.issues_view);
        mIssues.setHasFixedSize(true);
        mIssues.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mIssuesAdapter = new IssuesAdapter();
        mIssues.setAdapter(mIssuesAdapter);
*/

        issueViewAdapter = new IssueViewAdapter(getContext(), issueModelArrayList);
        mViewPager.setAdapter(issueViewAdapter);
      //  indicator.setupWithViewPager(mViewPager, true);




        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // TODO Auto-generated method stub
                mExapandableAdapter = null;

                issueGroupModelArrayList = issueModelArrayList.get(position).getGroupModelArrayList();

                mExapandableAdapter = new ExpListAdapterFrequentAsked(getContext(), issueGroupModelArrayList);
                expandableListView.setAdapter(mExapandableAdapter);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                // TODO Auto-generated method stub

                System.out.println("onPageScrolled");
            }

            @Override
            public void onPageScrollStateChanged(int num) {
                // TODO Auto-generated method stub


            }
        });


        expandableListView.setDivider(getResources().getDrawable(R.color.white));
        expandableListView.setChildDivider(getResources().getDrawable(R.color.white));


        ViewCompat.setNestedScrollingEnabled(dwdsd, true);


        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }
        mSearch.setVisibility(View.VISIBLE);
        getIssues(search);

        return view;
    }


    private void getIssues(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();

        GetIssues getIssues = new GetIssues(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("issues");

                        JSONArray jsonArray = object.getJSONArray("data");


                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = jsonArray.getJSONObject(i);

                            IssueModel issueModel = new IssueModel();
                            issueModel.setIssue_id(object1.getString("issue_id"));
                            issueModel.setTitle(object1.getString(session.getTitle()));
                            issueModel.setDescription(object1.getString(session.getDescription()));
                            issueModel.setBanner(object1.getString("banner"));

                            JSONArray jsonArray1 = object1.getJSONArray("faq");

                            ArrayList<IssueGroupModel> groupArrayList = new ArrayList<>();


                            for (int j = 0; j < jsonArray1.length(); j++){
                                JSONObject object2 = jsonArray1.getJSONObject(j);
                                ArrayList<IssueChildModel> childArrayList = new ArrayList<>();

                                IssueChildModel issueChildModel = new IssueChildModel();
                                issueChildModel.setAnswer(object2.getString(session.getAnswer()));

                                IssueGroupModel issueGroupModel = new IssueGroupModel();
                                issueGroupModel.setQuestion(object2.getString(session.getQuestion()));


                                childArrayList.add(issueChildModel);

                                issueGroupModel.setIssueChildModelArrayList(childArrayList);

                                groupArrayList.add(issueGroupModel);

                            }

                            issueModel.setGroupModelArrayList(groupArrayList);

                            issueModelArrayList.add(issueModel);


                        }

                        Log.e("daaaata", issueModelArrayList.get(0).getDescription()     + "---");

                        issueViewAdapter.notifyDataSetChanged();

                        mExapandableAdapter = null;

                        issueGroupModelArrayList = issueModelArrayList.get(0).getGroupModelArrayList();

                        mExapandableAdapter = new ExpListAdapterFrequentAsked(getContext(), issueGroupModelArrayList);
                        expandableListView.setAdapter(mExapandableAdapter);

                    }

                } catch (Exception e) {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getIssues);


    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }
}

