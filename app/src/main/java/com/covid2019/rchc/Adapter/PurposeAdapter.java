package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.R;

public class PurposeAdapter extends RecyclerView.Adapter<PurposeAdapter.Viewholder> {

    private Context context;
    private int[] colors = {R.color.stitle2, R.color.stitle3, R.color.stitle4, R.color.stitle1, R.color.stitle2, R.color.stitle3, R.color.stitle4, R.color.stitle1};
    private String[] text;

    public PurposeAdapter(Context context) {
        this.context = context;
        text = new String[]{context.getString(R.string.education), context.getString(R.string.health), context.getString(R.string.security), context.getString(R.string.politics), context.getString(R.string.technology), context.getString(R.string.commerce), context.getString(R.string.religion), context.getString(R.string.civics)};
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_purpose, null);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.textpurpose.setText(text[position]);
        holder.background.setBackgroundResource(colors[position]);

        if (position == 7){
            holder.line1.setVisibility(View.GONE);
            holder.line2.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return 8;
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView textpurpose;
        RelativeLayout background;
        ImageView line1, line2;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            textpurpose = itemView.findViewById(R.id.textpurpose);
            background = itemView.findViewById(R.id.background);
            line1 = itemView.findViewById(R.id.line1);
            line2 = itemView.findViewById(R.id.line2);
        }
    }

}
