package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.SeriesInnerModel;
import com.covid2019.rchc.Model.SeriesModel;
import com.covid2019.rchc.R;

import java.util.ArrayList;

public class SeriesAdapter extends RecyclerView.Adapter<SeriesAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<SeriesModel> seriesModelArrayList;
    private SeriesInnerAdapter seriesInnerAdapter;
    private final OnItemClickListener mListener;

    public SeriesAdapter(Context context, ArrayList<SeriesModel> seriesModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.seriesModelArrayList = seriesModelArrayList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public SeriesAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.adapter_series, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SeriesAdapter.MyViewHolder holder, int position) {

        holder.seriesTitle.setText(seriesModelArrayList.get(position).getTitle());

        holder.recInnerSeries.setLayoutManager(new GridLayoutManager(context, 2));

        seriesInnerAdapter = new SeriesInnerAdapter(context, seriesModelArrayList.get(position).getSeriesInnerModelArrayList(), new SeriesInnerAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ArrayList<SeriesInnerModel> innerModelArrayList, int positionInner) {
                mListener.onItemClick(innerModelArrayList, position, positionInner);
            }
        });

        holder.recInnerSeries.setAdapter(seriesInnerAdapter);

    }

    @Override
    public int getItemCount() {
        return seriesModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView seriesTitle;
        RecyclerView recInnerSeries;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            seriesTitle = itemView.findViewById(R.id.seriesTitle);
            recInnerSeries = itemView.findViewById(R.id.recInnerSeries);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(ArrayList<SeriesInnerModel> innerModelArrayList, int position, int positionInner);
    }
}
