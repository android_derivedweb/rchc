package com.covid2019.rchc.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.R;

    public class ProductsAdapter extends RecyclerView.Adapter<com.covid2019.rchc.Adapter.ProductsAdapter.MyViewHolder> {


        @Override
        public ProductsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
            View view = layoutInflater.inflate(R.layout.adapter_products, parent, false);
            return new ProductsAdapter.MyViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ProductsAdapter.MyViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return 4;
        }

        public class MyViewHolder extends RecyclerView.ViewHolder {
            public MyViewHolder( View itemView) {
                super(itemView);
            }
        }


    }