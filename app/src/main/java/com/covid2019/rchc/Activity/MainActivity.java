package com.covid2019.rchc.Activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Api.PostDeviceToken;
import com.covid2019.rchc.Fragments.Fragment_Avtar;
import com.covid2019.rchc.Fragments.Fragment_Character;
import com.covid2019.rchc.Fragments.Fragment_Comic;
import com.covid2019.rchc.Fragments.Fragment_Home;
import com.covid2019.rchc.Fragments.Fragment_Issues;
import com.covid2019.rchc.Fragments.Fragment_Literature;
import com.covid2019.rchc.Fragments.Fragment_Our_Work;
import com.covid2019.rchc.Fragments.Fragment_Proadcast;
import com.covid2019.rchc.Fragments.Fragment_Projects;
import com.covid2019.rchc.Fragments.Fragment_Purpose;
import com.covid2019.rchc.Fragments.Fragment_Research;
import com.covid2019.rchc.Fragments.Fragment_Series;
import com.covid2019.rchc.Fragments.Fragment_Services;
import com.covid2019.rchc.Fragments.Fragment_Setting;
import com.covid2019.rchc.Fragments.Fragment_Story;
import com.covid2019.rchc.Fragments.Fragment_Video;
import com.covid2019.rchc.Fragments.Fragment_Vision;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.infideap.drawerbehavior.AdvanceDrawerLayout;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    private LinearLayout fragmentLinearHome ;
    private ImageView btnNavigationDrawer, logo;
    private TextView ourworkbtn, servicesbtn,productsbtn, issuesbtn, researchNav, projectsNav, characterNav, seriesNav, clickProdCast;

    private LinearLayout expandLayoutActivities, expandLayoutResources, expandLayoutProfiles, expandLayoutLegends;
    private RelativeLayout clickActivities, clickResources, clickProfiles, clickLegends, clickComic;
    private ImageView drawerMinusA, drawerPlusA, drawerMinusR, drawerPlusR, drawerMinusP, drawerPlusP, drawerMinusL, drawerPlusL;

    private boolean activities = true;
    private boolean resources = true;
    private boolean profiles = true;
    private boolean legends = true;
    public static AdvanceDrawerLayout drawer;
    private boolean mFragmentHomeOpen = false;
    private boolean mFragmentOurWordOpen = false;
    private boolean mFragmentServiceOpen = false;
    private boolean mFragmentProductOpen = false;
    private boolean mFragmentIssuesOpen = false;
    private boolean mFragmentResearchOpen = false;
    private boolean mFragmentProjectOpen = false;
    private boolean mFragmentCharcterOpen = false;
    private boolean mFragmentSeriesOpen = false;
    private boolean mFragmentStoryOpen = false;
    private boolean mFragmentVisionOpen = false;
    private boolean mFragmentPurposeOpen = false;
    private boolean mFragmentAvtarOpen = false;
    private boolean mFragmentVideoOpen = false;
    private boolean mFragmentProdcastOpen = false;
    private boolean mFragmentSettingOpen = false;
    private boolean mFragmentLiteratureOpen = false;
    private EditText search_txt;
    public static ImageView mSearch;
    private RequestQueue requestQueue;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.drawerlayout);

        final ImageView drawer_menu = findViewById(R.id.btnNavigationDrawer);
        drawer = findViewById(R.id.drawer_layout);
        drawer.useCustomBehavior(Gravity.START);
        drawer.useCustomBehavior(Gravity.END);


        drawer_menu.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RtlHardcoded")
            @Override
            public void onClick(View v) {
                drawer.openDrawer(Gravity.LEFT);
            }
        });
        requestQueue = Volley.newRequestQueue(MainActivity.this);//Creating the RequestQueue

        drawer.refreshDrawableState();

        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());

        DeviceToken(userSession.getDeviceToken());
        search_txt = findViewById(R.id.search_txt);
        mSearch = findViewById(R.id.mSearch);
        expandLayoutActivities = findViewById(R.id.expandLayoutActivities);
        clickActivities = findViewById(R.id.clickActivities);
        drawerMinusA = findViewById(R.id.drawerMinus);
        drawerPlusA = findViewById(R.id.drawerPlus);

        expandLayoutResources = findViewById(R.id.expandLayoutResources);
        drawerMinusR = findViewById(R.id.drawerMinusR);
        drawerPlusR = findViewById(R.id.drawerPlusR);
        clickResources = findViewById(R.id.clickResources);

        clickComic = findViewById(R.id.clickComic);

        expandLayoutProfiles = findViewById(R.id.expandLayoutProfiles);
        clickProfiles = findViewById(R.id.clickProfiles);
        drawerMinusP = findViewById(R.id.drawerMinusP);
        drawerPlusP = findViewById(R.id.drawerPlusP);

        expandLayoutLegends = findViewById(R.id.expandLayoutLegends);
        clickLegends = findViewById(R.id.clickLegends);
        drawerMinusL = findViewById(R.id.drawerMinusL);
        drawerPlusL = findViewById(R.id.drawerPlusL);

        clickActivities.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (activities) {
                    drawerMinusA.setVisibility(View.VISIBLE);
                    drawerPlusA.setVisibility(View.GONE);

                    expandLayoutActivities.setVisibility(View.VISIBLE);
                    expandLayoutResources.setVisibility(View.GONE);
                    expandLayoutProfiles.setVisibility(View.GONE);
                    expandLayoutLegends.setVisibility(View.GONE);

                    drawerMinusR.setVisibility(View.GONE);
                    drawerPlusR.setVisibility(View.VISIBLE);
                    drawerMinusP.setVisibility(View.GONE);
                    drawerPlusP.setVisibility(View.VISIBLE);
                    drawerMinusL.setVisibility(View.GONE);
                    drawerPlusL.setVisibility(View.VISIBLE);

                    activities = false;
                    resources = true;
                    profiles  = true;
                    legends = true;

                } else {
                    drawerMinusA.setVisibility(View.GONE);
                    drawerPlusA.setVisibility(View.VISIBLE);
                    expandLayoutActivities.setVisibility(View.GONE);

                    activities = true;
                    resources = true;
                    profiles = true;
                    legends = true;
                }
            }
        });

        clickResources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (resources) {
                    drawerMinusR.setVisibility(View.VISIBLE);
                    drawerPlusR.setVisibility(View.GONE);

                    expandLayoutResources.setVisibility(View.VISIBLE);
                    expandLayoutActivities.setVisibility(View.GONE);
                    expandLayoutProfiles.setVisibility(View.GONE);
                    expandLayoutLegends.setVisibility(View.GONE);

                    drawerMinusA.setVisibility(View.GONE);
                    drawerPlusA.setVisibility(View.VISIBLE);
                    drawerMinusP.setVisibility(View.GONE);
                    drawerPlusP.setVisibility(View.VISIBLE);
                    drawerMinusL.setVisibility(View.GONE);
                    drawerPlusL.setVisibility(View.VISIBLE);


                    resources = false;
                    activities = true;
                    profiles = true;
                    legends = true;

                } else {
                    drawerMinusR.setVisibility(View.GONE);
                    drawerPlusR.setVisibility(View.VISIBLE);
                    expandLayoutResources.setVisibility(View.GONE);

                    resources = true;
                    activities = true;
                    profiles = true;
                    legends = true;
                }
            }
        });

        clickProfiles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (profiles) {
                    drawerMinusP.setVisibility(View.VISIBLE);
                    drawerPlusP.setVisibility(View.GONE);

                    expandLayoutProfiles.setVisibility(View.VISIBLE);
                    expandLayoutResources.setVisibility(View.GONE);
                    expandLayoutActivities.setVisibility(View.GONE);
                    expandLayoutLegends.setVisibility(View.GONE);

                    drawerMinusR.setVisibility(View.GONE);
                    drawerPlusR.setVisibility(View.VISIBLE);
                    drawerMinusA.setVisibility(View.GONE);
                    drawerPlusA.setVisibility(View.VISIBLE);
                    drawerMinusL.setVisibility(View.GONE);
                    drawerPlusL.setVisibility(View.VISIBLE);


                    profiles = false;
                    resources = true;
                    activities = true;
                    legends = true;

                } else {
                    drawerMinusP.setVisibility(View.GONE);
                    drawerPlusP.setVisibility(View.VISIBLE);
                    expandLayoutProfiles.setVisibility(View.GONE);

                    profiles = true;
                    resources = true;
                    activities = true;
                    legends = true;
                }
            }
        });

        clickLegends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (legends) {
                    drawerMinusL.setVisibility(View.VISIBLE);
                    drawerPlusL.setVisibility(View.GONE);

                    expandLayoutLegends.setVisibility(View.VISIBLE);
                    expandLayoutProfiles.setVisibility(View.GONE);
                    expandLayoutResources.setVisibility(View.GONE);
                    expandLayoutActivities.setVisibility(View.GONE);


                    drawerMinusA.setVisibility(View.GONE);
                    drawerPlusA.setVisibility(View.VISIBLE);
                    drawerMinusP.setVisibility(View.GONE);
                    drawerPlusP.setVisibility(View.VISIBLE);
                    drawerMinusR.setVisibility(View.GONE);
                    drawerPlusR.setVisibility(View.VISIBLE);

                    legends = false;
                    resources = true;
                    activities = true;
                    profiles = true;

                } else {
                    drawerMinusL.setVisibility(View.GONE);
                    drawerPlusL.setVisibility(View.VISIBLE);
                    expandLayoutLegends.setVisibility(View.GONE);


                    legends = true;
                    profiles = true;
                    resources = true;
                    activities = true;
                }
            }
        });



        fragmentLinearHome = findViewById(R.id.fragmentLinearHome);
        logo = findViewById(R.id.logo);
        ourworkbtn = findViewById(R.id.ourworkbtn);
        servicesbtn = findViewById(R.id.servicesbtn);
        productsbtn = findViewById(R.id.productsbtn);
        issuesbtn = findViewById(R.id.issuesbtn);
        researchNav = findViewById(R.id.researchNav);
        projectsNav = findViewById(R.id.projectsNav);
        characterNav = findViewById(R.id.characterNav);
        seriesNav = findViewById(R.id.seriesNav);

        btnNavigationDrawer = findViewById(R.id.btnNavigationDrawer);


        mFragmentHomeOpen = true;
        mFragmentOurWordOpen = false;
        mFragmentServiceOpen = false;
        mFragmentProductOpen = false;
        mFragmentIssuesOpen = false;
        mFragmentResearchOpen = false;
        mFragmentProjectOpen = false;
        mFragmentCharcterOpen = false;
        mFragmentSeriesOpen = false;
        mFragmentStoryOpen = false;
        mFragmentVisionOpen = false;
        mFragmentPurposeOpen = false;
        mFragmentAvtarOpen = false;
        mFragmentVideoOpen = false;
        mFragmentProdcastOpen = false;
        mFragmentSettingOpen = false;
        mFragmentLiteratureOpen = false;

        Fragment_Home fragment_home = new Fragment_Home();
        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "HomeFrag");

        clickComic.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                findViewById(R.id.search_bar).setVisibility(View.GONE);

                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = true;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Comic fragment_comic = new Fragment_Comic("fromComic");
                replaceFragment(R.id.fragmentLinearHome, fragment_comic, "ComicFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        } );

        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mFragmentHomeOpen = true;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;

                Fragment_Home fragment_home = new Fragment_Home();
                replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");


            }
        });

        ourworkbtn.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("RtlHardcoded")
            @Override
            public void onClick(View v) {

                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = true;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Our_Work fragment_our_work = new Fragment_Our_Work();
                replaceFragment(R.id.fragmentLinearHome, fragment_our_work, "OurWorkFrag");
                drawer.closeDrawer(Gravity.LEFT);


            }
        });

        servicesbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = true;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Services fragment_services = new Fragment_Services();
                replaceFragment(R.id.fragmentLinearHome, fragment_services, "ServicesFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
       } );

        productsbtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = true;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
               /* Fragment_Products fragment_products = new Fragment_Products();
                replaceFragment(R.id.fragmentLinearHome, fragment_products, "ProductsFrag");*/
                Fragment_Comic fragment_comic = new Fragment_Comic("fromProducts");
                replaceFragment(R.id.fragmentLinearHome, fragment_comic, "ComicFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        } );

        issuesbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = true;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Issues fragment_issues = new Fragment_Issues();
                replaceFragment(R.id.fragmentLinearHome, fragment_issues, "IssuesFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        researchNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = true;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Research fragment_research = new Fragment_Research();
                replaceFragment(R.id.fragmentLinearHome, fragment_research, "ResearchFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        projectsNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = true;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Projects fragment_projects = new Fragment_Projects();
                replaceFragment(R.id.fragmentLinearHome, fragment_projects, "ProjectsFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        characterNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = true;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Character fragment_character = new Fragment_Character();
                replaceFragment(R.id.fragmentLinearHome, fragment_character, "CharacterFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        seriesNav.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = true;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Series fragment_series = new Fragment_Series();
                replaceFragment(R.id.fragmentLinearHome, fragment_series, "SeriesFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });


        findViewById(R.id.mStory).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                findViewById(R.id.search_bar).setVisibility(View.GONE);

                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = true;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Story fragment_story = new Fragment_Story();
                replaceFragment(R.id.fragmentLinearHome, fragment_story, "StoryFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.mVision).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = true;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Vision fragment_vision = new Fragment_Vision();
                replaceFragment(R.id.fragmentLinearHome, fragment_vision, "VisionFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.mPurpose).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = true;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Purpose fragment_purpose = new Fragment_Purpose();
                replaceFragment(R.id.fragmentLinearHome, fragment_purpose, "PurposeFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.mAvtar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = true;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Avtar fragment_avtar = new Fragment_Avtar();
                replaceFragment(R.id.fragmentLinearHome, fragment_avtar, "AvatarFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.clickVideos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = true;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;

                Fragment_Video fragment_video = new Fragment_Video();
                replaceFragment(R.id.fragmentLinearHome, fragment_video, "VideoFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.clickProdCast).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = true;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = false;
                Fragment_Proadcast fragment_proadcast = new Fragment_Proadcast();
                replaceFragment(R.id.fragmentLinearHome, fragment_proadcast, "ProdCastFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.mSetting).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = true;
                mFragmentLiteratureOpen = false;
                Fragment_Setting fragment_setting = new Fragment_Setting();
                replaceFragment(R.id.fragmentLinearHome, fragment_setting, "SettingFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.mlit).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFragmentHomeOpen = false;
                mFragmentOurWordOpen = false;
                mFragmentServiceOpen = false;
                mFragmentProductOpen = false;
                mFragmentIssuesOpen = false;
                mFragmentResearchOpen = false;
                mFragmentProjectOpen = false;
                mFragmentCharcterOpen = false;
                mFragmentSeriesOpen = false;
                mFragmentStoryOpen = false;
                mFragmentVisionOpen = false;
                mFragmentPurposeOpen = false;
                mFragmentAvtarOpen = false;
                mFragmentVideoOpen = false;
                mFragmentProdcastOpen = false;
                mFragmentSettingOpen = false;
                mFragmentLiteratureOpen = true;
                Fragment_Literature fragment_literature = new Fragment_Literature();
                replaceFragment(R.id.fragmentLinearHome, fragment_literature, "LiteratureFrag");
                drawer.closeDrawer(Gravity.LEFT);

            }
        });

        findViewById(R.id.mSearch).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                findViewById(R.id.search_bar).setVisibility(View.VISIBLE);

            }
        });

        findViewById(R.id.search_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mFragmentHomeOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Home fragment_home = new Fragment_Home();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");
                }else if(mFragmentAvtarOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Avtar fragment_home = new Fragment_Avtar();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentCharcterOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Character fragment_home = new Fragment_Character();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentIssuesOpen){

                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Issues fragment_home = new Fragment_Issues();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentLiteratureOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Literature fragment_home = new Fragment_Literature();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentProdcastOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Proadcast fragment_home = new Fragment_Proadcast();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentProjectOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Projects fragment_home = new Fragment_Projects();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentSeriesOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Series fragment_home = new Fragment_Series();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentServiceOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Services fragment_home = new Fragment_Services();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentStoryOpen){

                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Story fragment_home = new Fragment_Story();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentVideoOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Video fragment_home = new Fragment_Video();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }else if(mFragmentVisionOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Vision fragment_home = new Fragment_Vision();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                } else if(mFragmentResearchOpen){
                    Bundle bundle = new Bundle();
                    bundle.putString("search",search_txt.getText().toString());
                    Fragment_Research fragment_home = new Fragment_Research();
                    fragment_home.setArguments(bundle);
                    replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                }
            }
        });

        search_txt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if(s.length()==0){
                    findViewById(R.id.search_bar).setVisibility(View.GONE);

                    if(mFragmentHomeOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Home fragment_home = new Fragment_Home();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");
                    }else if(mFragmentAvtarOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Avtar fragment_home = new Fragment_Avtar();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentCharcterOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Character fragment_home = new Fragment_Character();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentIssuesOpen){

                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Issues fragment_home = new Fragment_Issues();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentLiteratureOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Literature fragment_home = new Fragment_Literature();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentProdcastOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Proadcast fragment_home = new Fragment_Proadcast();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentProjectOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Projects fragment_home = new Fragment_Projects();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentSeriesOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Series fragment_home = new Fragment_Series();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentServiceOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Services fragment_home = new Fragment_Services();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentStoryOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Story fragment_home = new Fragment_Story();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentVideoOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Video fragment_home = new Fragment_Video();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }else if(mFragmentVisionOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Vision fragment_home = new Fragment_Vision();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    } else if(mFragmentResearchOpen){
                        Bundle bundle = new Bundle();
                        bundle.putString("search","");
                        Fragment_Research fragment_home = new Fragment_Research();
                        fragment_home.setArguments(bundle);
                        replaceFragmentHome(R.id.fragmentLinearHome, fragment_home, "LogoFrag");

                    }
                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        Intent iin= getIntent();
        Bundle b = iin.getExtras();
        if(b!=null)
        {
            String j =(String) b.get("type");
            Log.e("type",j);
        }

        if(UserSession.TYPE.contains("article")){
            UserSession.TYPE = "";
            Fragment_Literature fragment_literature = new Fragment_Literature();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_literature, "LogoFrag");
        }else if(UserSession.TYPE.contains("avatar")){
            UserSession.TYPE = "";
            Fragment_Avtar fragment_avtar = new Fragment_Avtar();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_avtar, "LogoFrag");
        }else if(UserSession.TYPE.contains("book")){
            UserSession.TYPE = "";
            Fragment_Literature fragment_literature = new Fragment_Literature();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_literature, "LogoFrag");
        }else if(UserSession.TYPE.contains("character")){
            UserSession.TYPE = "";
            Fragment_Character fragment_character = new Fragment_Character();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_character, "LogoFrag");
        }else if(UserSession.TYPE.contains("comic")){
            UserSession.TYPE = "";
            Fragment_Comic fragment_character = new Fragment_Comic("fromProducts");
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_character, "LogoFrag");
        }else if(UserSession.TYPE.contains("issue")){
            UserSession.TYPE = "";
            Fragment_Issues  fragment_issues = new Fragment_Issues();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_issues, "LogoFrag");
        }else if(UserSession.TYPE.contains("journal")){
            UserSession.TYPE = "";
            Fragment_Literature fragment_literature = new Fragment_Literature();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_literature, "LogoFrag");
        }else if(UserSession.TYPE.contains("prodcast")){
            UserSession.TYPE = "";
            Fragment_Proadcast fragment_proadcast = new Fragment_Proadcast();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_proadcast, "LogoFrag");
        }else if(UserSession.TYPE.contains("project")){
            UserSession.TYPE = "";
            Fragment_Projects fragment_projects = new Fragment_Projects();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_projects, "LogoFrag");
        }else if(UserSession.TYPE.contains("series")){
            UserSession.TYPE = "";
            Fragment_Series fragment_series = new Fragment_Series();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_series, "LogoFrag");
        }else if(UserSession.TYPE.contains("series-video")){
            UserSession.TYPE = "";
            Fragment_Video fragment_services = new Fragment_Video();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_services, "LogoFrag");
        }else if(UserSession.TYPE.contains("service")){
            UserSession.TYPE = "";
            Fragment_Services fragment_services = new Fragment_Services();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_services, "LogoFrag");
        }else if(UserSession.TYPE.contains("video")){
            UserSession.TYPE = "";
            Fragment_Video fragment_services = new Fragment_Video();
            replaceFragmentHome(R.id.fragmentLinearHome, fragment_services, "LogoFrag");
        }
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        if (getSupportFragmentManager().getBackStackEntryCount() >= 0) {
            getSupportFragmentManager().popBackStack();
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(containerViewId, fragment, fragmentTag)
                    .addToBackStack(fragmentTag)
                    .commit();
        }
    }

    protected void replaceFragmentHome(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .disallowAddToBackStack()
                .commit();
    }

    @Override
    public void onBackPressed() {
        if (this.drawer.isDrawerOpen(GravityCompat.START)) {
            this.drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    public void replaceFragment(Fragment fragment, Bundle bundle) {

        fragment.setArguments(bundle);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.fragmentLinearHome, fragment);
        transaction.commit();

    }
    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());
    }
    private void DeviceToken(String deviceToke){
        final KProgressHUD progressDialog = KProgressHUD.create(MainActivity.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        PostDeviceToken getAvatars = new PostDeviceToken(deviceToke,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");
                progressDialog.dismiss();


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getAvatars);


    }

}