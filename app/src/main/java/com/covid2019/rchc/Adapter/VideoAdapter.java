package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.covid2019.rchc.Model.VideoModel;
import com.covid2019.rchc.R;

import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.Viewholder> {

    private Context context;
    private ArrayList<VideoModel> videoModelArrayList;
    private final OnItemClickListener mListener;

    public VideoAdapter(Context context, ArrayList<VideoModel> videoModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.videoModelArrayList = videoModelArrayList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.video_adapter, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        Glide.with(context).load(videoModelArrayList.get(position).getVideo()).into(holder.imageVideo);


        holder.titleVideo.setText(videoModelArrayList.get(position).getTitle());
        holder.desVideo.setText(videoModelArrayList.get(position).getDescription());
        holder.dateVideo.setText(videoModelArrayList.get(position).getDate());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(videoModelArrayList.get(position).getVideo_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return videoModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView imageVideo;
        TextView titleVideo, desVideo, dateVideo;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            imageVideo = itemView.findViewById(R.id.imageVideo);
            titleVideo = itemView.findViewById(R.id.titleVideo);
            desVideo = itemView.findViewById(R.id.desVideo);
            dateVideo = itemView.findViewById(R.id.dateVideo);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(String id);
    }


}
