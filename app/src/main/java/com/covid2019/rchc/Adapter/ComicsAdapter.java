package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.ComicsModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ComicsAdapter extends RecyclerView.Adapter<ComicsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<ComicsModel> comicsModelArrayList;

    public ComicsAdapter(Context context, ArrayList<ComicsModel> comicsModelArrayList) {
        this.context = context;
        this.comicsModelArrayList = comicsModelArrayList;
    }

    @Override
    public ComicsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.adapter_proadcast, parent, false);
        return new ComicsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ComicsAdapter.MyViewHolder holder, int position) {

        holder.proad_text.setText(comicsModelArrayList.get(position).getTitle());

        Picasso.get().load(comicsModelArrayList.get(position).getBanner()).into(holder.proad_img);

    }


    @Override
    public int getItemCount() {
        return comicsModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView proad_img;
        TextView proad_text;

        public MyViewHolder( View itemView) {
            super(itemView);
            proad_img = itemView.findViewById(R.id.proad_img);
            proad_text = itemView.findViewById(R.id.proad_text);
        }
    }


}