package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.CharacterModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CharacterAdapter extends RecyclerView.Adapter<CharacterAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<CharacterModel> characterModelArrayList;

    public CharacterAdapter(Context context, ArrayList<CharacterModel> characterModelArrayList) {
        this.context = context;
        this.characterModelArrayList = characterModelArrayList;
    }

    @NonNull
    @Override
    public CharacterAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.adapter_character, parent, false);
        return new CharacterAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CharacterAdapter.MyViewHolder holder, int position) {

        Picasso.get().load(characterModelArrayList.get(position).getBanner()).into(holder.char_img);

        holder.titleChar.setText(characterModelArrayList.get(position).getTitle());
        holder.charTitle1.setText(characterModelArrayList.get(position).getTitle1());
        holder.charTitle2.setText(characterModelArrayList.get(position).getTitle2());

    }

    @Override
    public int getItemCount() {
        return characterModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView char_img;
        TextView titleChar, charTitle1, charTitle2;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            char_img = itemView.findViewById(R.id.char_img);
            titleChar = itemView.findViewById(R.id.titleChar);
            charTitle1 = itemView.findViewById(R.id.charTitle1);
            charTitle2 = itemView.findViewById(R.id.charTitle2);
        }

    }
}
