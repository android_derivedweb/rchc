package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.SeriesInnerModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SeriesInnerAdapter extends RecyclerView.Adapter<SeriesInnerAdapter.Viewholder> {

    private Context context;
    private ArrayList<SeriesInnerModel> innerModelArrayList;
    private final OnItemClickListener mListener;

    public SeriesInnerAdapter(Context context, ArrayList<SeriesInnerModel> innerModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.innerModelArrayList = innerModelArrayList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_series_inner, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.series_name.setText(innerModelArrayList.get(position).getTitle());
        holder.seriesDes.setText(innerModelArrayList.get(position).getDescription());

        Picasso.get().load(innerModelArrayList.get(position).getBanner()).into(holder.series_img);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(innerModelArrayList, position);
            }
        });


    }

    @Override
    public int getItemCount() {
        return innerModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView series_img;
        TextView series_name, seriesDes;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            series_img = itemView.findViewById(R.id.series_img);
            series_name = itemView.findViewById(R.id.series_name);
            seriesDes = itemView.findViewById(R.id.seriesDes);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(ArrayList<SeriesInnerModel> innerModelArrayList, int positionInner);
    }
}
