package com.covid2019.rchc.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.covid2019.rchc.R;

public class LiteratureDetailsActivity extends AppCompatActivity {

    private ImageView bannerImage, serviceImage;
    private TextView titleDetail, description1, description2;
    private CardView cardvViewDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_literature_details);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        bannerImage = findViewById(R.id.bannerImage);
        serviceImage = findViewById(R.id.serviceImage);
        titleDetail = findViewById(R.id.titleDetail);
        description1 = findViewById(R.id.description1);
        description2 = findViewById(R.id.description2);
        cardvViewDetails = findViewById(R.id.cardvViewDetails);

        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        String identLite = getIntent().getStringExtra("identLite");

        if (identLite.equals("journals")){
            Glide.with(LiteratureDetailsActivity.this).load(getIntent().getStringExtra("bannerLite")).into(bannerImage);
            titleDetail.setText(getIntent().getStringExtra("titleLite"));
            description1.setText(getIntent().getStringExtra("desLite"));
            Glide.with(LiteratureDetailsActivity.this).load(getIntent().getStringExtra("imageLite")).into(serviceImage);
            description2.setText(getIntent().getStringExtra("des2Lite"));


        } else if (identLite.equals("articals")){
            Glide.with(LiteratureDetailsActivity.this).load(getIntent().getStringExtra("bannerLite")).into(bannerImage);
            titleDetail.setText(getIntent().getStringExtra("titleLite"));
            description1.setText(getIntent().getStringExtra("desLite"));
            Glide.with(LiteratureDetailsActivity.this).load(getIntent().getStringExtra("imageLite")).into(serviceImage);
            description2.setText(getIntent().getStringExtra("des2Lite"));


        } else if (identLite.equals("books")){
            Glide.with(LiteratureDetailsActivity.this).load(getIntent().getStringExtra("bannerLite")).into(bannerImage);
            titleDetail.setText(getIntent().getStringExtra("titleLite"));
            description1.setText(getIntent().getStringExtra("desLite"));
            Glide.with(LiteratureDetailsActivity.this).load(getIntent().getStringExtra("imageLite")).into(serviceImage);
            description2.setText(getIntent().getStringExtra("des2Lite"));
        }


        if (getIntent().getStringExtra("imageLite").equals("null")){
            cardvViewDetails.setVisibility(View.GONE);
        }

    }
}