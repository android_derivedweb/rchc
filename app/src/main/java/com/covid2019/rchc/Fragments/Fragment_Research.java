package com.covid2019.rchc.Fragments;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Activity.HomeDetail;
import com.covid2019.rchc.Adapter.ResearchAdapter;
import com.covid2019.rchc.Api.GetResearch;
import com.covid2019.rchc.Model.ServicesModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Research extends Fragment {

    private RecyclerView recResearch;
    private ResearchAdapter researchAdapter;

    private ArrayList<ServicesModel> researchModelArrayList = new ArrayList<>();
    private RequestQueue requestQueue;
    private String search="";

    private UserSession session;

    public Fragment_Research() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_research, container, false);

        session = new UserSession(getContext());

        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        recResearch = view.findViewById(R.id.recResearch);

        recResearch.setLayoutManager(new LinearLayoutManager(getContext()));
        researchAdapter = new ResearchAdapter(getContext(), researchModelArrayList, new ResearchAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String id) {

                Intent intent = new Intent(getContext(), HomeDetail.class);
                intent.putExtra("researchId", id);
                intent.putExtra("identifier", "Research");
                startActivity(intent);
            }
        });
        recResearch.setAdapter(researchAdapter);
        researchAdapter.notifyDataSetChanged();

        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }
        mSearch.setVisibility(View.VISIBLE);
        getResearch(search);

        return view;
    }

    private void getResearch(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetResearch getResearch = new GetResearch(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = jsonArray.getJSONObject(i);

                            ServicesModel servicesModel = new ServicesModel();
                            servicesModel.setService_id(object1.getString("research_id"));
                            servicesModel.setTitle(object1.getString(session.getTitle()));
                            servicesModel.setDescription(object1.getString(session.getDescription()));
                            servicesModel.setBanner(object1.getString("banner"));

                            researchModelArrayList.add(servicesModel);

                        }

                        Log.e("dfshjhff", researchModelArrayList.get(0).getDescription() + "---");


                        researchAdapter.notifyDataSetChanged();

                    }

                } catch (Exception e) {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getResearch);


    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }
}