package com.covid2019.rchc.Model;

public class SeriesInnerModel {

    String series_video_id;
    String series_id;
    String title;
    String banner;
    String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getSeries_video_id() {
        return series_video_id;
    }

    public void setSeries_video_id(String series_video_id) {
        this.series_video_id = series_video_id;
    }

    public String getSeries_id() {
        return series_id;
    }

    public void setSeries_id(String series_id) {
        this.series_id = series_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
