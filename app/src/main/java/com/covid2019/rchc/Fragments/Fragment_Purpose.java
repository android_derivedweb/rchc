package com.covid2019.rchc.Fragments;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Adapter.PurposeAdapter;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;

import java.util.Locale;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Purpose extends Fragment {

    private PurposeAdapter purposeAdapter;
    private RecyclerView recPurpose;



    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_fragment_purpose, container, false);

        recPurpose = view.findViewById(R.id.recPurpose);
        mSearch.setVisibility(View.GONE);
        recPurpose.setLayoutManager(new LinearLayoutManager(getContext()));
        purposeAdapter = new PurposeAdapter(getContext());
        recPurpose.setAdapter(purposeAdapter);


        return view;
    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }
}