package com.covid2019.rchc.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.covid2019.rchc.Adapter.SeriesViewAdapter;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;

import java.util.ArrayList;
import java.util.Locale;

public class SeriesDetails extends AppCompatActivity {

    private RelativeLayout mBackBtn;
    private SeriesViewAdapter seriesViewAdapter;
    private ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_series_details);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        mViewPager = (ViewPager) findViewById(R.id.viewPagerSeries);

        mBackBtn = findViewById(R.id.back_btn);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        ArrayList<String> imagesArray = getIntent().getExtras().getStringArrayList("imagesArray");
        ArrayList<String> descriptionArray = getIntent().getExtras().getStringArrayList("descriptionArray");
        int posInner = getIntent().getIntExtra("posInner", 0);


        seriesViewAdapter = new SeriesViewAdapter(SeriesDetails.this, imagesArray, descriptionArray);
        mViewPager.setAdapter(seriesViewAdapter);


        mViewPager.setCurrentItem(posInner, true);


        findViewById(R.id.previousSeries).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(getItem(-1), true);
            }
        });

        findViewById(R.id.nextSeries).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mViewPager.setCurrentItem(getItem(+1), true);
            }
        });
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());


    }


    private int getItem(int i) {
        return mViewPager.getCurrentItem() + i;
    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());
    }
}