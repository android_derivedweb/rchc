package com.covid2019.rchc.Model;

import java.util.ArrayList;

public class IssueGroupModel {

    String question;
    ArrayList<IssueChildModel> issueChildModelArrayList;

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public ArrayList<IssueChildModel> getIssueChildModelArrayList() {
        return issueChildModelArrayList;
    }

    public void setIssueChildModelArrayList(ArrayList<IssueChildModel> issueChildModelArrayList) {
        this.issueChildModelArrayList = issueChildModelArrayList;
    }
}
