package com.covid2019.rchc.Api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.covid2019.rchc.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

public class PostDeviceToken extends StringRequest {

    private Map<String, String> parameters;


    public PostDeviceToken(String search, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, UserSession.BASEURL + "add-device-details", listener, errorListener);
        parameters = new HashMap<>();
         parameters.put("device_type", "android" );
         parameters.put("device_token", search );


        Log.e("fdfd", UserSession.BASEURL + "---");
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {

    return parameters;
    }

}