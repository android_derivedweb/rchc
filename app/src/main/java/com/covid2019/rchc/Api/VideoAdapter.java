package com.covid2019.rchc.Api;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.covid2019.rchc.Model.VideosModel;
import com.covid2019.rchc.R;

import java.util.ArrayList;

public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.Viewholder> {

    private Context context;
    private ArrayList<VideosModel> videosModelArrayList;
    private final OnItemClickListener mListener;

    public VideoAdapter(Context context, ArrayList<VideosModel> videosModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.videosModelArrayList = videosModelArrayList;
        this.mListener = listener;

    }

    @NonNull
    @Override
    public VideoAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_video, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoAdapter.Viewholder holder, int position) {

        holder.titleProdVideo.setText(videosModelArrayList.get(position).getTitle());

        holder.txt.setText(videosModelArrayList.get(position).getDescription());
        holder.dateVoice.setText(videosModelArrayList.get(position).getDate());

        Glide.with(context).load(videosModelArrayList.get(position).getVideo()).into(holder.videoProd);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(videosModelArrayList.get(position).getVideo_id());
            }
        });



    }

    @Override
    public int getItemCount() {
        return videosModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView videoProd;
        TextView titleProdVideo, txt, dateVoice;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            videoProd = itemView.findViewById(R.id.videoProd);
            titleProdVideo = itemView.findViewById(R.id.titleProdVideo);
            txt = itemView.findViewById(R.id.txt);
            dateVoice = itemView.findViewById(R.id.dateVoice);

        }
    }


    public interface OnItemClickListener {
        void onItemClick(String id);
    }



}
