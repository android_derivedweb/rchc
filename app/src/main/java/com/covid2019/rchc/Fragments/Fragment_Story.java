package com.covid2019.rchc.Fragments;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Api.GetStory;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Locale;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Story extends Fragment {

    private ImageView featured_img;
    private TextView founderName, postDetails, details1, details2, details3;
    private RequestQueue requestQueue;
    private String search = "";

    private UserSession session;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_fragment_story, container, false);
        session = new UserSession(getContext());

        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        featured_img = view.findViewById(R.id.featured_img);
        founderName = view.findViewById(R.id.founderName);
        postDetails = view.findViewById(R.id.postDetails);
        details1 = view.findViewById(R.id.details1);
        details2 = view.findViewById(R.id.details2);
        details3 = view.findViewById(R.id.details3);

        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }

        mSearch.setVisibility(View.GONE);
        getStory(search);


        return view;
    }

    private void getStory(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetStory getStory = new GetStory(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        Picasso.get().load(object.getString("profile_pic")).into(featured_img);



                        if (session.getLanguageCode().equals("en")) {
                            founderName.setText(object.getString("name"));
                            postDetails.setText(object.getString("post_details"));
                            details1.setText(object.getString("details_1"));
                            details2.setText(object.getString("details_2"));
                            details3.setText(object.getString("details_3"));
                        } else if (session.getLanguageCode().equals("fr")){
                            founderName.setText(object.getString("name_fr"));
                            postDetails.setText(object.getString("post_details_fr"));
                            details1.setText(object.getString("details_1_fr"));
                            details2.setText(object.getString("details_2_fr"));
                            details3.setText(object.getString("details_3_fr"));
                        } else if (session.getLanguageCode().equals("rw")){
                            founderName.setText(object.getString("name_rw"));
                            postDetails.setText(object.getString("post_details_rw"));
                            details1.setText(object.getString("details_1_rw"));
                            details2.setText(object.getString("details_2_rw"));
                            details3.setText(object.getString("details_3_rw"));
                        } else if (session.getLanguageCode().equals("kln")){
                            founderName.setText(object.getString("name_ln"));
                            postDetails.setText(object.getString("post_details_ln"));
                            details1.setText(object.getString("details_1_ln"));
                            details2.setText(object.getString("details_2_ln"));
                            details3.setText(object.getString("details_3_ln"));
                        } else if (session.getLanguageCode().equals("rn")){
                            founderName.setText(object.getString("name_rn"));
                            postDetails.setText(object.getString("post_details_rn"));
                            details1.setText(object.getString("details_1_rn"));
                            details2.setText(object.getString("details_2_rn"));
                            details3.setText(object.getString("details_3_rn"));
                        } else if (session.getLanguageCode().equals("sw")){
                            founderName.setText(object.getString("name_sw"));
                            postDetails.setText(object.getString("post_details_sw"));
                            details1.setText(object.getString("details_1_sw"));
                            details2.setText(object.getString("details_2_sw"));
                            details3.setText(object.getString("details_3_sw"));
                        }

                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getStory);


    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }

}