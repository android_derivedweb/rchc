package com.covid2019.rchc.Fragments;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Api.GetComics;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONObject;

import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Comic extends Fragment {

    private RequestQueue requestQueue;
    private WebView webView1;
    private String valueFrom;

    public Fragment_Comic(String valueFrom) {
        this.valueFrom = valueFrom;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_comic, container, false);

        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        mSearch.setVisibility(View.GONE);

        webView1 = view.findViewById(R.id.webView1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        getWebUrl();

        return view;
    }

    private void getWebUrl(){
        final KProgressHUD progressDialog = KProgressHUD.create(getActivity())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetComics getComics = new GetComics(new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdfftgrtrtdf", response + "---");


                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseComic", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        String comicProductURL = null;

                        if (valueFrom.equals("fromComic")) {
                            comicProductURL = object.getString("comics_url");
                        } else if (valueFrom.equals("fromProducts")){
                            comicProductURL = object.getString("product_url");
                        }

                        WebSettings settings = webView1.getSettings();

                        settings.setJavaScriptEnabled(true);
                        webView1.setScrollBarStyle(View.SCROLLBARS_OUTSIDE_OVERLAY);

                        webView1.getSettings().setBuiltInZoomControls(true);
                        webView1.getSettings().setUseWideViewPort(true);
                        webView1.getSettings().setLoadWithOverviewMode(true);

                        webView1.setWebViewClient(new WebViewClient() {
                            @Override
                            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                                view.loadUrl(url);
                                return true;
                            }

                            @Override
                            public void onPageFinished(WebView view, String url) {
                                if (progressDialog.isShowing()) {
                                    progressDialog.dismiss();
                                }
                            }

                            @Override
                            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                                Toast.makeText(getContext(), "Error:" + description, Toast.LENGTH_SHORT).show();

                            }
                        });
                        webView1.loadUrl(comicProductURL);

                    }

                } catch (Exception e) {
                    Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

              //  Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getComics);


    }
    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
    @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }

}
