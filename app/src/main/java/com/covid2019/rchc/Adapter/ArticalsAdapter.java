package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.PopularModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ArticalsAdapter extends RecyclerView.Adapter<ArticalsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<PopularModel> articalsModelArrayList;
    private final OnItemClickListener mListener;

    public ArticalsAdapter(Context context, ArrayList<PopularModel> articalsModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.articalsModelArrayList = articalsModelArrayList;
        this.mListener = listener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.adapter_articals, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.title.setText(articalsModelArrayList.get(position).getTitle());
        holder.txt.setText(articalsModelArrayList.get(position).getDescription());

        Picasso.get().load(articalsModelArrayList.get(position).getBanner()).into(holder.proad_img);



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(articalsModelArrayList.get(position).getBanner_thumb(),
                        articalsModelArrayList.get(position).getTitle(),
                        articalsModelArrayList.get(position).getDescription(),
                        articalsModelArrayList.get(position).getImage_thumb(),
                        articalsModelArrayList.get(position).getDescription2());
            }
        });

    }


    @Override
    public int getItemCount() {
        return articalsModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView proad_img;
        TextView title, txt;

        public MyViewHolder( View itemView) {
            super(itemView);

            proad_img = itemView.findViewById(R.id.proad_img);
            title = itemView.findViewById(R.id.title);
            txt = itemView.findViewById(R.id.txt);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(String banner, String title, String des, String image, String des2);
    }


}

