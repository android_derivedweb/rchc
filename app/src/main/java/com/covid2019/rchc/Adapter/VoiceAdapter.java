package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.covid2019.rchc.Model.VideosModel;
import com.covid2019.rchc.R;

import java.util.ArrayList;

public class VoiceAdapter extends RecyclerView.Adapter<VoiceAdapter.Viewholder> {

    private Context context;
    private ArrayList<VideosModel> voiceModelArrayList;
    private final OnItemClickListener listener;
    private int index = -1;

    public VoiceAdapter(Context context, ArrayList<VideosModel> voiceModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.voiceModelArrayList = voiceModelArrayList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public VoiceAdapter.Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_voice, parent, false);
        return new VoiceAdapter.Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull VoiceAdapter.Viewholder holder, int position) {

        holder.titleProdVideo.setText(voiceModelArrayList.get(position).getTitle());

        holder.txt.setText(voiceModelArrayList.get(position).getDescription());
        holder.dateVoice.setText(voiceModelArrayList.get(position).getDate());

        Glide.with(context).load(voiceModelArrayList.get(position).getBanner()).into(holder.audioProd);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onItemClick(position);
            }
        });

        holder.imgPlayPause.setImageResource(voiceModelArrayList.get(position).isPlaying ? R.drawable.pause : R.drawable.play);

        if(index == position){

        }else {
            holder.imgPlayPause.setImageResource(R.drawable.play);
        }

    }

    @Override
    public int getItemCount() {
        return voiceModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView audioProd, imgPlayPause;
        TextView titleProdVideo, txt, dateVoice;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            audioProd = itemView.findViewById(R.id.audioProd);
            titleProdVideo = itemView.findViewById(R.id.titleProdVideo);
            txt = itemView.findViewById(R.id.txt);
            dateVoice = itemView.findViewById(R.id.dateVoice);
            imgPlayPause = itemView.findViewById(R.id.imgPlayPause);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(int item);
    }

    public void filterList(int pos) {
        index = pos;
        if(voiceModelArrayList.get(pos).getIsPlaying()){
            voiceModelArrayList.get(pos).setIsPlaying(false);
        }else {
            voiceModelArrayList.get(pos).setIsPlaying(true);
        }

        notifyDataSetChanged();
    }


}
