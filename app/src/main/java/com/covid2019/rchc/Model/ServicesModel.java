package com.covid2019.rchc.Model;

public class ServicesModel {

    String service_id;
    String title;
    String title_fr;
    String title_rw;
    String title_ln;
    String title_rn;
    String title_sw;
    String description;
    String description_fr;
    String description_rw;
    String description_ln;
    String description_rn;
    String description_sw;
    String banner;

    public String getTitle_fr() {
        return title_fr;
    }

    public void setTitle_fr(String title_fr) {
        this.title_fr = title_fr;
    }

    public String getTitle_rw() {
        return title_rw;
    }

    public void setTitle_rw(String title_rw) {
        this.title_rw = title_rw;
    }

    public String getTitle_ln() {
        return title_ln;
    }

    public void setTitle_ln(String title_ln) {
        this.title_ln = title_ln;
    }

    public String getTitle_rn() {
        return title_rn;
    }

    public void setTitle_rn(String title_rn) {
        this.title_rn = title_rn;
    }

    public String getTitle_sw() {
        return title_sw;
    }

    public void setTitle_sw(String title_sw) {
        this.title_sw = title_sw;
    }

    public String getDescription_fr() {
        return description_fr;
    }

    public void setDescription_fr(String description_fr) {
        this.description_fr = description_fr;
    }

    public String getDescription_rw() {
        return description_rw;
    }

    public void setDescription_rw(String description_rw) {
        this.description_rw = description_rw;
    }

    public String getDescription_ln() {
        return description_ln;
    }

    public void setDescription_ln(String description_ln) {
        this.description_ln = description_ln;
    }

    public String getDescription_rn() {
        return description_rn;
    }

    public void setDescription_rn(String description_rn) {
        this.description_rn = description_rn;
    }

    public String getDescription_sw() {
        return description_sw;
    }

    public void setDescription_sw(String description_sw) {
        this.description_sw = description_sw;
    }

    public String getService_id() {
        return service_id;
    }

    public void setService_id(String service_id) {
        this.service_id = service_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
