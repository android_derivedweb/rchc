package com.covid2019.rchc.Fragments;

import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Activity.HomeDetail;
import com.covid2019.rchc.Adapter.PopularAdapter;
import com.covid2019.rchc.Api.GetHome;
import com.covid2019.rchc.Model.PopularModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Home extends Fragment {

    private RecyclerView mPopular;
    private PopularAdapter mPopularAdapter;
    private CardView mFeaturedCard;

    private ImageView featuredImg;
    private TextView featuredDes;
    private RequestQueue requestQueue;

    private UserSession session;

    private ArrayList<PopularModel> popularModelArrayList = new ArrayList<>();
    private String search = "";
    private String service_id;

    public Fragment_Home() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_fragment_home, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue
        session = new UserSession(getContext());


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        mPopular = view.findViewById(R.id.popular_view);
        featuredImg = view.findViewById(R.id.featuredImg);
        featuredDes = view.findViewById(R.id.featuredDes);

        mPopular.setLayoutManager(new LinearLayoutManager(getContext()));
        mPopularAdapter = new PopularAdapter(getContext(), popularModelArrayList, new PopularAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String id) {
                Intent intent = new Intent(getContext(), HomeDetail.class);
                intent.putExtra("identifier", "Service");
                intent.putExtra("service_id", id);
                startActivity(intent);
            }
        });
        mPopular.setAdapter(mPopularAdapter);

        mFeaturedCard = view.findViewById(R.id.featured_card);
        mFeaturedCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), HomeDetail.class);
                intent.putExtra("identifier", "Service");
                intent.putExtra("service_id", service_id);
                startActivity(intent);
            }
        });


        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }
        mSearch.setVisibility(View.VISIBLE);
        getHome(search);

        return view;
    }

    private void getHome(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetHome getHome = new GetHome(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("featured");

                        service_id = object.getString("service_id");

                        featuredDes.setText(object.getString(session.getDescription()));


                        Picasso.get().load(object.getString("banner")).into(featuredImg);

                        JSONObject jsonObject1 = jsonObject.getJSONObject("popular");

                        JSONArray jsonArray = jsonObject1.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                            PopularModel popularModel = new PopularModel();
                            popularModel.setBanner(jsonObject2.getString("banner_thumb"));
                            popularModel.setTitle(jsonObject2.getString(session.getTitle()));

                            popularModel.setDescription(jsonObject2.getString(session.getDescription()));

                            popularModel.setService_id(jsonObject2.getString("service_id"));
                            popularModel.setDate(jsonObject2.getString("date"));

                            popularModelArrayList.add(popularModel);

                        }

                        mPopularAdapter.notifyDataSetChanged();

                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getHome);


    }
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
    @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }

}