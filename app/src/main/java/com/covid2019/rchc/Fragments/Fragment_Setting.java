package com.covid2019.rchc.Fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.covid2019.rchc.Activity.SettingDetails;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;

import java.util.Locale;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Setting extends Fragment {


    private TextView language_text;

    public Fragment_Setting() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_fragment_setting, container, false);

        mSearch.setVisibility(View.GONE);
        UserSession userSession = new UserSession(getActivity());
        language_text = view.findViewById(R.id.language_text);
        language_text.setText(userSession.getLanguage());
        view.findViewById(R.id.mChange_language).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getContext(), SettingDetails.class);
                startActivity(intent);
            }
        });

        return view;
    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
        language_text.setText(userSession.getLanguage());
    }
}