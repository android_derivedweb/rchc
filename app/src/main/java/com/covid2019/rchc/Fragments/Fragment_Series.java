package com.covid2019.rchc.Fragments;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Activity.SeriesDetails;
import com.covid2019.rchc.Adapter.SeriesAdapter;
import com.covid2019.rchc.Api.GetSeries;
import com.covid2019.rchc.Model.SeriesInnerModel;
import com.covid2019.rchc.Model.SeriesModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Series extends Fragment {

    private RecyclerView recSeries;
    private SeriesAdapter seriesAdapter;
    private RequestQueue requestQueue;
    private ArrayList<SeriesModel> seriesModelArrayList = new ArrayList<>();
    private String search = "";

    private UserSession session;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_series, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        session = new UserSession(getContext());


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        recSeries = view.findViewById(R.id.recSeries);

        recSeries.setLayoutManager(new LinearLayoutManager(getContext()));
        seriesAdapter = new SeriesAdapter(getContext(), seriesModelArrayList, new SeriesAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(ArrayList<SeriesInnerModel> innerModelArrayList, int pos, int posInner) {

                ArrayList<String> imagesArray = new ArrayList<>();
                ArrayList<String> descriptionArray = new ArrayList<>();

                for (int i = 0; i < innerModelArrayList.size(); i++){
                    imagesArray.add(innerModelArrayList.get(i).getBanner());
                    descriptionArray.add(innerModelArrayList.get(i).getDescription());
                }

                Intent intent = new Intent(getContext(), SeriesDetails.class);
                intent.putStringArrayListExtra("imagesArray", imagesArray);
                intent.putStringArrayListExtra("descriptionArray", descriptionArray);
                intent.putExtra("posInner", posInner);
                startActivity(intent);
            }
        });
        recSeries.setAdapter(seriesAdapter);


        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }
        mSearch.setVisibility(View.VISIBLE);
        getSeries(search);

        return view;
    }


    private void getSeries(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetSeries getSeries = new GetSeries(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject object1 = jsonArray.getJSONObject(i);

                            SeriesModel seriesModel = new SeriesModel();
                            seriesModel.setSeries_id(object1.getString("series_id"));
                            seriesModel.setTitle(object1.getString(session.getTitle()));

                            JSONArray jsonArray1 = object1.getJSONArray("series_videos");
                            ArrayList<SeriesInnerModel> innerModelArrayList = new ArrayList<>();

                            for(int j = 0; j < jsonArray1.length(); j++){
                                JSONObject object2 = jsonArray1.getJSONObject(j);

                                SeriesInnerModel seriesInnerModel = new SeriesInnerModel();
                                seriesInnerModel.setSeries_video_id(object2.getString("series_video_id"));
                                seriesInnerModel.setSeries_id(object2.getString("series_id"));
                                seriesInnerModel.setTitle(object2.getString(session.getTitle()));
                                seriesInnerModel.setDescription(object2.getString(session.getDescription()));
                                seriesInnerModel.setBanner(object2.getString("banner"));

                                innerModelArrayList.add(seriesInnerModel);
                            }

                            seriesModel.setSeriesInnerModelArrayList(innerModelArrayList);
                            seriesModelArrayList.add(seriesModel);

                        }

                        seriesAdapter.notifyDataSetChanged();

                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getSeries);


    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }

}