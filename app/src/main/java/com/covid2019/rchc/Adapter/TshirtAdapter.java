package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.R;

public class TshirtAdapter extends RecyclerView.Adapter<com.covid2019.rchc.Adapter.TshirtAdapter.MyViewHolder> {

    private String mValue;
    private final OnItemClickListener mListener;
    private Context context;


    public TshirtAdapter(String value, Context context , OnItemClickListener listener) {
        this.mValue = value;
        this.context = context;
        this.mListener = listener;
    }

    @Override
    public TshirtAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.adapter_tshirts, parent, false);
        return new TshirtAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TshirtAdapter.MyViewHolder holder, int position) {


        if(mValue == "0"){
            holder.tShirt.setImageDrawable(holder.tShirt.getContext().getResources().getDrawable(R.drawable.tsirt2));
            holder.text_tshirt.setText("Black Hoodie");
        }else{
            holder.tShirt.setImageDrawable(holder.tShirt.getContext().getResources().getDrawable(R.drawable.comic1));
            holder.text_tshirt.setText("Comic 1");
        }

        holder.tshirt_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    mListener.onItemClick(position);
                }
        });


    }

    @Override
    public int getItemCount() {
        return 4;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView tShirt;
        TextView text_tshirt;
        LinearLayout tshirt_buy;

        public MyViewHolder( View itemView) {
            super(itemView);
            tShirt = itemView.findViewById(R.id.tShirt);
            text_tshirt = itemView.findViewById(R.id.text_tshirt);
            tshirt_buy = itemView.findViewById(R.id.tshirt_buy);
        }
    }


    public interface OnItemClickListener {
        void onItemClick(int item);
    }

}