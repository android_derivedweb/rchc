package com.covid2019.rchc.Activity;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.covid2019.rchc.Adapter.RelatedVideosAdapter;
import com.covid2019.rchc.Api.GetVideoDetails;
import com.covid2019.rchc.Model.VideoModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.ct7ct7ct7.androidvimeoplayer.view.VimeoPlayerView;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class VideoDetails extends AppCompatActivity {

    private RequestQueue requestQueue;
    private ImageView imageVideo;
    private TextView titleVideo, desVideo, dateVideo;
    private RecyclerView resRelatedVideos;
    private RelatedVideosAdapter relatedVideosAdapter;
    private ArrayList<VideoModel> videoModelArrayList = new ArrayList<>();
    // private VideoView videoView;
    private MediaController mediaController;
    private UserSession session;
    private VimeoPlayerView vimeoPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_details);

        session = new UserSession(VideoDetails.this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        requestQueue = Volley.newRequestQueue(VideoDetails.this);//Creating the RequestQueue

        imageVideo = findViewById(R.id.imageVideo);
        titleVideo = findViewById(R.id.titleVideo);
        desVideo = findViewById(R.id.desVideo);
        dateVideo = findViewById(R.id.dateVideo);
        resRelatedVideos = findViewById(R.id.resRelatedVideos);


        findViewById(R.id.back_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        String videoId = getIntent().getStringExtra("videoId");


        resRelatedVideos.setLayoutManager(new LinearLayoutManager(VideoDetails.this));
        relatedVideosAdapter = new RelatedVideosAdapter(VideoDetails.this, videoModelArrayList, new RelatedVideosAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String id) {
                Intent intent = new Intent(VideoDetails.this, VideoDetails.class);
                intent.putExtra("videoId", id);
                startActivity(intent);
                finish();
            }
        });
        resRelatedVideos.setAdapter(relatedVideosAdapter);

         getVideoDetails(videoId);

        // videoView = findViewById(R.id.videoView);
        // mediaController = new MediaController(VideoDetails.this);
        // mediaController.setAnchorView(videoView);

        //specify the location of media file
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());

      /*  videoView.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                videoView.start(); //need to make transition seamless.
            }
        });*/

        vimeoPlayer = findViewById(R.id.vimeoPlayer);

//If video is open. but limit playing at embedded.
//        vimeoPlayer.initialize(true, {YourPrivateVideoId}, "SettingsEmbeddedUrl");

//If video is pirvate.
//        vimeoPlayer.initialize(true, {YourPrivateVideoId},"VideoHashKey", "SettingsEmbeddedUrl");


    }


    private void getVideoDetails(String videoId) {
        final KProgressHUD progressDialog = KProgressHUD.create(VideoDetails.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetVideoDetails getVideoDetails = new GetVideoDetails(videoId, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        Glide.with(VideoDetails.this).load(object.getString("banner")).into(imageVideo);

                        Uri uri = Uri.parse(object.getString("video"));

                        vimeoPlayer.initialize(Integer.parseInt(object.getString("vimeo_id")), object.getString("video_base_url"));


                        // Setting MediaController and URI, then starting the videoView
                        // videoView.setMediaController(mediaController);
                        //  videoView.setVideoURI(uri);
                        //  videoView.requestFocus();
                        // videoView.setMediaController(null);
                        // videoView.start();


                        titleVideo.setText(object.getString(session.getTitle()));
                        desVideo.setText(object.getString(session.getDescription()));
                        dateVideo.setText(object.getString("date"));

                        JSONArray jsonArray = jsonObject.getJSONArray("related_videos");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject object1 = jsonArray.getJSONObject(i);

                            VideoModel videoModel = new VideoModel();
                            videoModel.setVideo_id(object1.getString("video_id"));
                            videoModel.setVideo(object1.getString("banner"));
                            videoModel.setTitle(object1.getString(session.getTitle()));
                            videoModel.setCategory(object1.getString(session.getCategory()));
                            videoModel.setDescription(object1.getString(session.getDescription()));

                            videoModelArrayList.add(videoModel);

                        }


                        relatedVideosAdapter.notifyDataSetChanged();

                    }

                } catch (Exception e) {
                    Toast.makeText(VideoDetails.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getVideoDetails);


    }

    @Override
    protected void onStop() {
        super.onStop();
        //  videoView.stopPlayback();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //  videoView.stopPlayback();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // videoView.resume();
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());
    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }


}