package com.covid2019.rchc.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Api.GetResearchDetaiils;
import com.covid2019.rchc.Api.GetServicesDetaiils;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Locale;

public class HomeDetail extends AppCompatActivity {

    private RelativeLayout mBackBtn;
    private TextView title, subTitle, description;
    private ImageView serviceImage;

    private RequestQueue requestQueue;

    private String service_id;
    private String researchId;

    private String identifier;

    private UserSession session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_detail);

        session = new UserSession(HomeDetail.this);

        requestQueue = Volley.newRequestQueue(HomeDetail.this);//Creating the RequestQueue

        service_id = getIntent().getStringExtra("service_id");
        researchId = getIntent().getStringExtra("researchId");

        identifier = getIntent().getStringExtra("identifier");


        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());


        title = findViewById(R.id.title);
        subTitle = findViewById(R.id.subTitle);
        description = findViewById(R.id.description);
        serviceImage = findViewById(R.id.serviceImage);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        mBackBtn = findViewById(R.id.back_btn);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        switch (identifier) {
            case "Service":
                getServicesDetails(service_id);
                break;
            case "Research":
                getResearchDetails(researchId);
                break;

        }

    }


    private void getServicesDetails(String service_id){
        final KProgressHUD progressDialog = KProgressHUD.create(HomeDetail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetServicesDetaiils getServicesDetaiils = new GetServicesDetaiils(service_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("services");

                        title.setText(object.getString(session.getTitle()));
                        subTitle.setText(object.getString(session.getTitle()));
                        description.setText(object.getString(session.getDescription()));

                        Picasso.get().load(object.getString("banner")).into(serviceImage);

                    }

                } catch (Exception e) {
                    Toast.makeText(HomeDetail.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.e("onErrorResponse", error.getMessage());
                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getServicesDetaiils);


    }

    private void getResearchDetails(String research_id){
        final KProgressHUD progressDialog = KProgressHUD.create(HomeDetail.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetResearchDetaiils getResearchDetaiils = new GetResearchDetaiils(research_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseResearch", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        title.setText(object.getString(session.getTitle()));
                        subTitle.setText(object.getString(session.getTitle()));
                        description.setText(object.getString(session.getDescription()));

                        Picasso.get().load(object.getString("banner")).into(serviceImage);

                    }

                } catch (Exception e) {
                    Toast.makeText(HomeDetail.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getResearchDetaiils);


    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());
    }
}