package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.PopularModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class JournalsAdapter extends RecyclerView.Adapter<JournalsAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<PopularModel> journalsModelArrayList;
    private final OnItemClickListener mListener;

    public JournalsAdapter(Context context, ArrayList<PopularModel> journalsModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.journalsModelArrayList = journalsModelArrayList;
        this.mListener = listener;
    }

    @Override
    public JournalsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.adapter_literature, parent, false);
        return new JournalsAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(JournalsAdapter.MyViewHolder holder, int position) {

        holder.proad_text.setText(journalsModelArrayList.get(position).getTitle());

        Picasso.get().load(journalsModelArrayList.get(position).getBanner()).into(holder.proad_img);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(journalsModelArrayList.get(position).getBanner_thumb(),
                        journalsModelArrayList.get(position).getTitle(),
                        journalsModelArrayList.get(position).getDescription(),
                        journalsModelArrayList.get(position).getImage_thumb(),
                        journalsModelArrayList.get(position).getDescription2());
            }
        });


    }


    @Override
    public int getItemCount() {
        return journalsModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView proad_img;
        TextView proad_text;

        public MyViewHolder( View itemView) {
            super(itemView);

            proad_img = itemView.findViewById(R.id.proad_img);
            proad_text = itemView.findViewById(R.id.proad_text);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(String banner, String title, String des, String image, String des2);
    }

}