package com.covid2019.rchc.Model;

import java.util.ArrayList;

public class IssueModel {

    String issue_id;
    String title;
    String description;
    String banner;
    ArrayList<IssueGroupModel> groupModelArrayList;


    public ArrayList<IssueGroupModel> getGroupModelArrayList() {
        return groupModelArrayList;
    }

    public void setGroupModelArrayList(ArrayList<IssueGroupModel> groupModelArrayList) {
        this.groupModelArrayList = groupModelArrayList;
    }

    public String getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(String issue_id) {
        this.issue_id = issue_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }
}
