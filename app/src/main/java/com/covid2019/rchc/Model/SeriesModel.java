package com.covid2019.rchc.Model;

import java.util.ArrayList;

public class SeriesModel {

    String series_id;
    String title;
    ArrayList<SeriesInnerModel> seriesInnerModelArrayList;

    public String getSeries_id() {
        return series_id;
    }

    public void setSeries_id(String series_id) {
        this.series_id = series_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<SeriesInnerModel> getSeriesInnerModelArrayList() {
        return seriesInnerModelArrayList;
    }

    public void setSeriesInnerModelArrayList(ArrayList<SeriesInnerModel> seriesInnerModelArrayList) {
        this.seriesInnerModelArrayList = seriesInnerModelArrayList;
    }
}
