package com.covid2019.rchc.Api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.covid2019.rchc.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

public class GetIssues extends StringRequest {

    private Map<String, String> parameters;


    public GetIssues(String search,Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, UserSession.BASEURL + "get-issues?search="+search, listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );

        Log.e("fdfd", UserSession.BASEURL + "---");
    }

    @Override
    protected Map<String, String> getParams() throws AuthFailureError {

    return parameters;
    }

}