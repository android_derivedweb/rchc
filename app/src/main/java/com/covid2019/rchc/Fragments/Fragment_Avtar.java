package com.covid2019.rchc.Fragments;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Adapter.AvtarAdapter;
import com.covid2019.rchc.Api.GetAvatars;
import com.covid2019.rchc.Model.AvatarsModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class  Fragment_Avtar extends Fragment {

    private RecyclerView recAvatar;
    private AvtarAdapter mAvtarAdapter;
    private RequestQueue requestQueue;
    private ArrayList<AvatarsModel> avatarsModelArrayList = new ArrayList<>();
    private String search = "";

    private UserSession session;

    public Fragment_Avtar() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_fragment_avtar, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        session = new UserSession(getContext());

        mSearch.setVisibility(View.VISIBLE);
        recAvatar = view.findViewById(R.id.popular_view);
        recAvatar.setLayoutManager(new GridLayoutManager(getActivity(), 2));

        DividerItemDecoration verticalDecoration = new DividerItemDecoration(recAvatar.getContext(),
                DividerItemDecoration.HORIZONTAL);
        Drawable verticalDivider = ContextCompat.getDrawable(getActivity(), R.drawable.vertical_divider);
        verticalDecoration.setDrawable(verticalDivider);
        recAvatar.addItemDecoration(verticalDecoration);

        mAvtarAdapter = new AvtarAdapter(getContext(), avatarsModelArrayList);
        recAvatar.setAdapter(mAvtarAdapter);


        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }

        getAvatars(search);

        return view;
    }

    private void getAvatars(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetAvatars getAvatars = new GetAvatars(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject object1 = jsonArray.getJSONObject(i);
                            AvatarsModel avatarsModel = new AvatarsModel();
                            avatarsModel.setAvatar_id(object1.getString("avatar_id"));
                            avatarsModel.setTitle(object1.getString(session.getTitle()));
                            avatarsModel.setImage(object1.getString("image"));
                            avatarsModelArrayList.add(avatarsModel);
                        }

                        mAvtarAdapter.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getAvatars);


    }

    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf,dm);
    }

    @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }
}