package com.covid2019.rchc.Fragments;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.covid2019.rchc.Adapter.ProductsAdapter;
import com.covid2019.rchc.Adapter.TshirtAdapter;
import com.covid2019.rchc.Activity.ProductDetails;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;

import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Products extends Fragment {

    private RecyclerView recProduct , recTshirts, recComics;
    private ProductsAdapter mProductsAdapter;
    private TshirtAdapter mTshirtAdapter;
    private String mValue;


    public Fragment_Products() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_products, container, false);
        mSearch.setVisibility(View.GONE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }
        recProduct = view.findViewById(R.id.recProduct);
        recProduct.setHasFixedSize(true);
        recProduct.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mProductsAdapter = new ProductsAdapter() ;
        recProduct.setAdapter(mProductsAdapter);

        recTshirts = view.findViewById(R.id.recTshirts);
        recTshirts.setHasFixedSize(true);
        recTshirts.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mTshirtAdapter = new TshirtAdapter("0", getContext(), new TshirtAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                Intent intent = new Intent(getContext(), ProductDetails.class);
                startActivity(intent);
            }
        }) ;
        recTshirts.setAdapter(mTshirtAdapter);

        recComics = view.findViewById(R.id.recComics);
        recComics.setHasFixedSize(true);
        recComics.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mTshirtAdapter = new TshirtAdapter("1", getContext(), new TshirtAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {
                Intent intent = new Intent(getContext(), ProductDetails.class);
                startActivity(intent);
            }
        }) ;
        recComics.setAdapter(mTshirtAdapter);
        return view;
    }
    public void setLocale(String lang) {
        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
    @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }
}
