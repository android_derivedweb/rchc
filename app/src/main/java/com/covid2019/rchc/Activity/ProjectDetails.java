package com.covid2019.rchc.Activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Api.GetProjectsDetails;
import com.covid2019.rchc.Api.ProjectApply;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.Locale;

public class ProjectDetails extends AppCompatActivity {

    private RelativeLayout mBackBtn;
    private String project_id;
    private RequestQueue requestQueue;

    private TextView subTtlProject, ttlProject, desProject, goalProject;
    private ImageView imgProject, timeLineImage;

    private EditText nameProject, emailProject;
    private Button btnApply;

    private String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";

    private UserSession session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_project_details);

        requestQueue = Volley.newRequestQueue(ProjectDetails.this);//Creating the RequestQueue
        session = new UserSession(ProjectDetails.this);

        project_id = getIntent().getStringExtra("project_id");


        goalProject = findViewById(R.id.goalProject);
        subTtlProject = findViewById(R.id.subTtlProject);
        ttlProject = findViewById(R.id.ttlProject);
        desProject = findViewById(R.id.desProject);
        imgProject = findViewById(R.id.imgProject);
        timeLineImage = findViewById(R.id.timeLineImage);
        nameProject = findViewById(R.id.nameProject);
        emailProject = findViewById(R.id.emailProject);
        btnApply = findViewById(R.id.btnApply);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        mBackBtn = findViewById(R.id.back_btn);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnApply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (!emailProject.getText().toString().trim().matches(emailPattern)) {
                    Toast.makeText(ProjectDetails.this, "Enter valid email address", Toast.LENGTH_SHORT).show();
                } else if (nameProject.getText().toString().isEmpty()){
                    Toast.makeText(ProjectDetails.this, "Enter name", Toast.LENGTH_SHORT).show();
                } else {
                    applyProject(project_id, nameProject.getText().toString(), emailProject.getText().toString().trim());
                }
            }
        });


        setLocale(session.getLanguageCode());

        //fdfdfgg
        getProductDetails(project_id);

    }

    private void applyProject(String project_id, String name, String email) {
        final KProgressHUD progressDialog = KProgressHUD.create(ProjectDetails.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        ProjectApply projectApply = new ProjectApply(project_id, name, email, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseProjectApply", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        Toast.makeText(ProjectDetails.this, jsonObject.getString("ResponseMsg"), Toast.LENGTH_SHORT).show();
                        finish();

                    }

                } catch (Exception e) {
                    Toast.makeText(ProjectDetails.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(projectApply);



    }


    private void getProductDetails(String project_id){
        final KProgressHUD progressDialog = KProgressHUD.create(ProjectDetails.this)
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetProjectsDetails getProjectsDetails = new GetProjectsDetails(project_id, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");

                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseProjectDetails", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        ttlProject.setText(object.getString(session.getTitle()));
                        subTtlProject.setText("ACLTCS - " + object.getString(session.getTitle()));
                        if (session.getLanguageCode().equals("en")) {
                            goalProject.setText(object.getString("goal"));
                        } else if (session.getLanguageCode().equals("fr")){
                            goalProject.setText(object.getString("goal_fr"));
                        } else if (session.getLanguageCode().equals("rw")){
                            goalProject.setText(object.getString("goal_rw"));
                        } else if (session.getLanguageCode().equals("kln")){
                            goalProject.setText(object.getString("goal_ln"));
                        } else if (session.getLanguageCode().equals("sw")){
                            goalProject.setText(object.getString("goal_sw"));
                        } else if (session.getLanguageCode().equals("rn")){
                            goalProject.setText(object.getString("goal_rn"));
                        }

                        desProject.setText(object.getString(session.getDescription()));


                        Picasso.get().load(object.getString("banner")).into(imgProject);
                        Picasso.get().load(object.getString("timeline")).into(timeLineImage);

                    }

                } catch (Exception e) {
                    Toast.makeText(ProjectDetails.this, e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getProjectsDetails);


    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }


    @Override
    protected void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());
    }
}