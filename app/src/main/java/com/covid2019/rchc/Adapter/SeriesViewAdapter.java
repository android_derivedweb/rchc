package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class SeriesViewAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<String> imagesArray;
    private ArrayList<String> descriptionArray;

    public SeriesViewAdapter(Context context , ArrayList<String> imagesArray, ArrayList<String> descriptionArray) {
        this.mContext = context;
        this.descriptionArray = descriptionArray;
        this.imagesArray = imagesArray;
    }
  
    @Override  
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }  

    @Override  
    public Object instantiateItem(ViewGroup container, int position) {

        View view = LayoutInflater.from(mContext).inflate(R.layout.view_pager_series, container, false);

        ImageView imageSeriesPager = view.findViewById(R.id.imageSeriesPager);
        TextView desSeriesPager = view.findViewById(R.id.desSeriesPager);

        desSeriesPager.setText(descriptionArray.get(position));
        Picasso.get().load(imagesArray.get(position)).into(imageSeriesPager);

        ((ViewPager) container).addView(view, 0);

        return view;

    }  
  
    @Override  
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
  
    @Override  
    public int getCount() {  
        return descriptionArray.size();
    }

}