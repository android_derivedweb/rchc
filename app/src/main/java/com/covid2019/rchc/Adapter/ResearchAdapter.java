package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.ServicesModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ResearchAdapter extends RecyclerView.Adapter<ResearchAdapter.Viewholder> {

    private final OnItemClickListener mListener;
    private Context context;
    private ArrayList<ServicesModel> researchModelArrayList;


    public ResearchAdapter(Context context, ArrayList<ServicesModel> researchModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.mListener = listener;
        this.researchModelArrayList = researchModelArrayList;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_research, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.titleRes.setText(researchModelArrayList.get(position).getTitle());
        holder.descriRes.setText(researchModelArrayList.get(position).getDescription());

        Picasso.get().load(researchModelArrayList.get(position).getBanner()).into(holder.imageRes);

        holder.researchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(researchModelArrayList.get(position).getService_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return researchModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView titleRes, descriRes;
        TextView researchBtn;
        ImageView imageRes;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            titleRes = itemView.findViewById(R.id.titleRes);
            descriRes = itemView.findViewById(R.id.descriRes);
            imageRes = itemView.findViewById(R.id.imageRes);
            researchBtn = itemView.findViewById(R.id.researchBtn);
        }
    }

    public interface OnItemClickListener {
        void onItemClick(String id);
    }
}
