package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.ProjectsModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class ProjectsAdapter extends RecyclerView.Adapter<ProjectsAdapter.Viewholder> {

    private final OnItemClickListener mListener;
    private Context context;
    private ArrayList<ProjectsModel> projectsModelArrayList;

    public ProjectsAdapter(Context context, ArrayList<ProjectsModel> projectsModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.mListener = listener;
        this.projectsModelArrayList = projectsModelArrayList;

    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_projects, parent, false);
        return new Viewholder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.desProjects.setText(projectsModelArrayList.get(position).getDescription());
        holder.project.setText(projectsModelArrayList.get(position).getTitle());

        Picasso.get().load(projectsModelArrayList.get(position).getBanner()).into(holder.imageProject);

        holder.projectsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(projectsModelArrayList.get(position).getProject_id());
            }
        });


    }

    @Override
    public int getItemCount() {
        return projectsModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView desProjects, projectsBtn, project;
        ImageView imageProject;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            desProjects = itemView.findViewById(R.id.desProjects);
            projectsBtn = itemView.findViewById(R.id.projectsBtn);
            imageProject = itemView.findViewById(R.id.imageProject);
            project = itemView.findViewById(R.id.project);
        }

    }

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

}

