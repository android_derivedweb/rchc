package com.covid2019.rchc.Fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Activity.VideoDetails;
import com.covid2019.rchc.Adapter.VideoAdapter;
import com.covid2019.rchc.Api.GetVideos;
import com.covid2019.rchc.Model.VideoModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.EndlessRecyclerViewScrollListener;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Video extends Fragment {

    private RequestQueue requestQueue;
    private ArrayList<VideoModel> videoModelArrayList = new ArrayList<>();
    private VideoAdapter videoAdapter;
    private RecyclerView resVideos;
    private String search="";

    private UserSession session;


    // pagination
    private int last_size;
    private String Mpage = "1";
    private LinearLayoutManager linearlayout;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_fragment_video, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        session = new UserSession(getContext());

        resVideos = view.findViewById(R.id.resVideos);

        linearlayout = new LinearLayoutManager(getContext());
        resVideos.setLayoutManager(linearlayout);
        videoAdapter = new VideoAdapter(getContext(), videoModelArrayList, new VideoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String videoId) {
                Intent intent = new Intent(getContext(), VideoDetails.class);
                intent.putExtra("videoId", videoId);
                startActivity(intent);
            }
        });
        resVideos.setAdapter(videoAdapter);


        resVideos.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearlayout) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.e("PageStatus",page + "  " + last_size);
                if (page!=last_size){
                    Mpage = String.valueOf(page+1);

                    getVideos(search, Mpage);
                }
            }
        });



        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }
        mSearch.setVisibility(View.VISIBLE);


        getVideos(search, "1");


        return view;
    }

    private void getVideos(String search, String Mpage){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetVideos getVideos = new GetVideos(search, Mpage, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");
                        last_size = object.getInt("last_page");

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject object1 = jsonArray.getJSONObject(i);

                            VideoModel videoModel = new VideoModel();
                            videoModel.setVideo_id(object1.getString("video_id"));
                            videoModel.setVideo(object1.getString("banner"));
                            videoModel.setTitle(object1.getString(session.getTitle()));
                            videoModel.setCategory(object1.getString(session.getCategory()));
                            videoModel.setDescription(object1.getString(session.getDescription()));
                            videoModel.setDate(object1.getString("date"));

                            videoModelArrayList.add(videoModel);

                        }

                        videoAdapter.notifyDataSetChanged();

                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getVideos);


    }
    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }
}