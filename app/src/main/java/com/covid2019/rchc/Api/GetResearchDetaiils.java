package com.covid2019.rchc.Api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.covid2019.rchc.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

public class GetResearchDetaiils extends StringRequest {

    private Map<String, String> parameters;


    public GetResearchDetaiils(String research_id, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, UserSession.BASEURL + "get-research-details?research_id=" + research_id, listener, errorListener);

        parameters = new HashMap<>();
      //  parameters.put("service_id ", OldPassword );

        Log.e("fdfd", UserSession.BASEURL + "---");
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {

    return parameters;
    }

}