package com.covid2019.rchc.Activity;

import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Adapter.LanguageAdapter;
import com.covid2019.rchc.Model.LanguageModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;

import java.util.ArrayList;
import java.util.Locale;

public class SettingDetails extends AppCompatActivity {

    private RelativeLayout mBackBtn;
    private RecyclerView setting_language;
    private LanguageAdapter mLanguageAdapter;
    private ArrayList<LanguageModel> languageModels = new ArrayList<>();
    private UserSession userSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_details);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        mBackBtn = findViewById(R.id.back_btn);
        mBackBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        userSession = new UserSession(SettingDetails.this);
        LanguageModel languageModel = new LanguageModel();
        languageModel.setName("English");
        languageModel.setCode("en");
        languageModel.setTitle("title");
        languageModel.setDescription("description");
        languageModel.setDescription2("description_2");
        languageModel.setQuestion("question");
        languageModel.setAnswer("answer");
        languageModel.setCategory("category");
        languageModels.add(languageModel);

        LanguageModel languageModel1 = new LanguageModel();
        languageModel1.setName("French");
        languageModel1.setCode("fr");
        languageModel1.setTitle("title_fr");
        languageModel1.setDescription("description_fr");
        languageModel1.setDescription2("description_2_fr");
        languageModel1.setQuestion("question_fr");
        languageModel1.setAnswer("answer_fr");
        languageModel1.setCategory("category_fr");
        languageModels.add(languageModel1);

        LanguageModel languageModel12 = new LanguageModel();
        languageModel12.setName("Kinyarwanda");
        languageModel12.setCode("kln");
        languageModel12.setTitle("title_ln");
        languageModel12.setDescription("description_ln");
        languageModel12.setDescription2("description_2_ln");
        languageModel12.setQuestion("question_ln");
        languageModel12.setAnswer("answer_ln");
        languageModel12.setCategory("category_ln");
        languageModels.add(languageModel12);


        LanguageModel languageModel122 = new LanguageModel();
        languageModel122.setName("Swahili");
        languageModel122.setCode("sw");
        languageModel122.setTitle("title_sw");
        languageModel122.setDescription("description_sw");
        languageModel122.setDescription2("description_2_sw");
        languageModel122.setQuestion("question_sw");
        languageModel122.setAnswer("answer_sw");
        languageModel122.setCategory("category_sw");
        languageModels.add(languageModel122);


        setting_language = findViewById(R.id.setting_language);
        setting_language.setLayoutManager(new LinearLayoutManager(SettingDetails.this));
        mLanguageAdapter = new LanguageAdapter(SettingDetails.this, languageModels, new LanguageAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String name, String code, String title, String description, String description2, String question, String answer, String category) {
                userSession.setLanguageCode(code);
                userSession.setLanguageName(name);
                userSession.setTitle(title);
                userSession.setDescription(description);
                userSession.setDescription2(description2);
                userSession.setQuestion(question);
                userSession.setAnswer(answer);
                userSession.setCategory(category);
                mLanguageAdapter.notifyDataSetChanged();

                userSession.setIslanchange(true);

                
                try{

                   /* Intent mStartActivity = new Intent(SettingDetails.this, Splash.class);
                    int mPendingIntentId = 123456;
                    PendingIntent mPendingIntent = PendingIntent.getActivity(SettingDetails.this, mPendingIntentId, mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                    AlarmManager mgr = (AlarmManager) SettingDetails.this.getSystemService(Context.ALARM_SERVICE);
                    mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 10, mPendingIntent);
                    System.exit(0);*/

                    PackageManager packageManager = getPackageManager();
                    Intent intent = packageManager.getLaunchIntentForPackage(getPackageName());
                    ComponentName componentName = intent.getComponent();
                    Intent mainIntent = Intent.makeRestartActivityTask(componentName);
                    startActivity(mainIntent);
                    Runtime.getRuntime().exit(0);



                   /* Intent intent = new Intent(SettingDetails.this, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    finish();*/


                }catch (Exception e) {
                    e.printStackTrace();
                }
                // after on CLick we are using finish to close and then just after that
                // we are calling startactivity(getIntent()) to open our application

            }
        });
        setting_language.setAdapter(mLanguageAdapter);
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());

    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }

    @Override
    protected void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(this);
        setLocale(userSession.getLanguageCode());
    }
}