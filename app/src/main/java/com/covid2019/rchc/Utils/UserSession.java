package com.covid2019.rchc.Utils;

import android.content.Context;
import android.content.SharedPreferences;

public class UserSession {

    public static String TYPE = "";
    SharedPreferences sharedPreferences;

    SharedPreferences.Editor editor;

    Context context;

    int PRIVATE_MODE = 0;

    public static String BASEURL = "http://chessmafia.com/php/covid19/api/";

    private static final String PREF_NAME = "UserSessionPref";

    private String KEY_FIREBASETOKEN = "KEY_FIREBASETOKEN";

    private static final String IS_LOGIN = "IsLoggedIn";
    private static final String LANGUAGENAME = "language";
    private static final String LANGUAGECODE = "languagecode";
    private static final String TITLE = "title";
    private static final String DESCRIPTION = "description";
    private static final String DESCRIPTION2 = "description_2";
    private static final String QUESTION = "question";
    private static final String ANSWER = "answer";
    private static final String CATEGORY = "category";
    private static final String ISLANCHANGE = "isLangChange";

    public UserSession(Context context) {

        sharedPreferences = context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = sharedPreferences.edit();
    }


    public void setLanguageName(String name){
        editor.putString(LANGUAGENAME,name);
        editor.commit();
    }/* public void setLanguageName(String name){
        editor.putString(LANGUAGENAME,name);
        editor.commit();
    }*/

    public String getLanguage(){
        return sharedPreferences.getString(LANGUAGENAME,"English");
    }

    public void setLanguageCode(String name){
        editor.putString(LANGUAGECODE,name);
        editor.commit();
    }

    public String getLanguageCode(){
        return sharedPreferences.getString(LANGUAGECODE,"en");
    }

    public void setTitle(String name){
        editor.putString(TITLE,name);
        editor.commit();
    }

    public void FirebaseToken(String Type){
        editor.putString(KEY_FIREBASETOKEN, Type);
        editor.commit();
    }


    public String getTitle(){
        return sharedPreferences.getString(TITLE,"title");
    }

    public void setDescription(String name){
        editor.putString(DESCRIPTION,name);
        editor.commit();
    }

    public String getDescription(){
        return sharedPreferences.getString(DESCRIPTION,"description");
    }

    public void setDescription2(String name){
        editor.putString(DESCRIPTION2,name);
        editor.commit();
    }

    public String getDescription2(){
        return sharedPreferences.getString(DESCRIPTION2,"description_2");
    }

    public void setQuestion(String name){
        editor.putString(QUESTION,name);
        editor.commit();
    }

    public String getQuestion(){
        return sharedPreferences.getString(QUESTION,"question");
    }

    public void setAnswer(String name){
        editor.putString(ANSWER,name);
        editor.commit();
    }

    public String getDeviceToken() {
        return sharedPreferences.getString(KEY_FIREBASETOKEN, "0");
    }

    public String getAnswer(){
        return sharedPreferences.getString(ANSWER,"answer");
    }

    public void setCategory(String name){
        editor.putString(CATEGORY,name);
        editor.commit();
    }

    public String getCategory(){
        return sharedPreferences.getString(CATEGORY,"category");
    }

    public void setIslanchange(boolean first){
        editor.putBoolean(ISLANCHANGE, first);
        editor.commit();
    }

    public boolean isLangChange(){
        return sharedPreferences.getBoolean(ISLANCHANGE,false);
    }

    public boolean isLoggedIn() {
        return sharedPreferences.getBoolean(IS_LOGIN, false);
    }

}
