package com.covid2019.rchc.Api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.covid2019.rchc.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

public class GetVideoDetails extends StringRequest {

    private Map<String, String> parameters;


    public GetVideoDetails(String videoId, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, UserSession.BASEURL + "get-video-details?video_id=" + videoId, listener, errorListener);
        parameters = new HashMap<>();
        // parameters.put("OldPassword ", OldPassword );

        Log.e("fdfd", UserSession.BASEURL + "---");
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {

    return parameters;
    }

}