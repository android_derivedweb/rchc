package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.AvatarsModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class AvtarAdapter extends RecyclerView.Adapter<AvtarAdapter.MyViewHolder> {

    private ArrayList<AvatarsModel> avatarsModelArrayList;
    private Context context;

    public AvtarAdapter(Context context, ArrayList<AvatarsModel> avatarsModelArrayList) {
        this.avatarsModelArrayList = avatarsModelArrayList;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.adapter_avtar, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, int position) {

        holder.profile_title.setText(avatarsModelArrayList.get(position).getTitle());

        Picasso.get().load(avatarsModelArrayList.get(position).getImage()).into(holder.profile_img);

        if (position % 2 == 0){
            holder.viewRight.setVisibility(View.VISIBLE);
            holder.viewLeft.setVisibility(View.GONE);
        } else {

            holder.viewRight.setVisibility(View.GONE);
            holder.viewLeft.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public int getItemCount() {
        return avatarsModelArrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        View viewRight, viewLeft;

        TextView profile_title;
        ImageView profile_img;

        public MyViewHolder( View itemView) {
            super(itemView);

            profile_title = itemView.findViewById(R.id.profile_title);
            viewLeft = itemView.findViewById(R.id.viewLeft);
            viewRight = itemView.findViewById(R.id.viewRight);
            profile_img = itemView.findViewById(R.id.profile_img);

        }
    }

}
