package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.covid2019.rchc.Model.IssueGroupModel;
import com.covid2019.rchc.R;

import java.util.ArrayList;

public class ExpListAdapterFrequentAsked extends BaseExpandableListAdapter {

    private Context _context;

    private ArrayList<IssueGroupModel> issueGroupModelArrayList = new ArrayList<>();

    public ExpListAdapterFrequentAsked(Context context, ArrayList<IssueGroupModel> issueGroupModelArrayList) {
        this._context = context;
        this.issueGroupModelArrayList = issueGroupModelArrayList;

    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return 1;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_item, null);
        }

        TextView askedFrequentListExp = (TextView) convertView.findViewById(R.id.expandedListItem);

        askedFrequentListExp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                askedFrequentListExp.setClickable(false);
            }
        });

        askedFrequentListExp.setText(issueGroupModelArrayList.get(groupPosition).getIssueChildModelArrayList().get(childPosition).getAnswer());

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return 1;
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.issueGroupModelArrayList.size();
    }

    @Override
    public int getGroupCount() {
        return issueGroupModelArrayList.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.list_group, null);
        }

        TextView txtListGroupFrequent = (TextView) convertView.findViewById(R.id.listTitle);
        ImageView imageView = (ImageView) convertView.findViewById(R.id.up);
        imageView.setSelected(isExpanded);

        txtListGroupFrequent.setText( issueGroupModelArrayList.get(groupPosition).getQuestion());


        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}