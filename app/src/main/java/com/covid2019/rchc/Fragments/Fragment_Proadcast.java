package com.covid2019.rchc.Fragments;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Activity.VideoDetails;
import com.covid2019.rchc.Adapter.ComicsAdapter;
import com.covid2019.rchc.Adapter.VoiceAdapter;
import com.covid2019.rchc.Api.GetProdcast;
import com.covid2019.rchc.Api.VideoAdapter;
import com.covid2019.rchc.Model.ComicsModel;
import com.covid2019.rchc.Model.VideosModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Proadcast extends Fragment {

    private RecyclerView recProadcast, recVideoProd, recVoiceProd;

    private ComicsAdapter mProadcastAdapter;
    private VideoAdapter videoAdapter;
    private VoiceAdapter voiceAdapter;
    private MediaPlayer mp;
    private int CurrentPosition = -1;

    private RequestQueue requestQueue;

    private ArrayList<ComicsModel> comicsModelArrayList = new ArrayList<>();
    private ArrayList<VideosModel> videosModelArrayList = new ArrayList<>();
    private ArrayList<VideosModel> voiceModelArrayList = new ArrayList<>();
    private String search = "";

    private UserSession session;

    public Fragment_Proadcast() {

    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_proadcast, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        session = new UserSession(getContext());

        mSearch.setVisibility(View.VISIBLE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        view.findViewById(R.id.seeAllVideos).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment_Video fragment_video = new Fragment_Video();
                replaceFragment(R.id.fragmentLinearHome, fragment_video, "VideoFrag");
            }
        });


        recProadcast = view.findViewById(R.id.recJournals);
        recProadcast.setHasFixedSize(true);
        recProadcast.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        mProadcastAdapter = new ComicsAdapter(getContext(), comicsModelArrayList);
        recProadcast.setAdapter(mProadcastAdapter);

        recVideoProd = view.findViewById(R.id.recVideoProd);
        recVideoProd.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        videoAdapter = new VideoAdapter(getContext(), videosModelArrayList, new VideoAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String id) {
                Intent intent = new Intent(getContext(), VideoDetails.class);
                intent.putExtra("videoId", id);
                startActivity(intent);
            }
        });
        recVideoProd.setAdapter(videoAdapter);


        recVoiceProd = view.findViewById(R.id.recVoiceProd);
        recVoiceProd.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        voiceAdapter = new VoiceAdapter(getContext(), voiceModelArrayList, new VoiceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(int item) {


                if(CurrentPosition == item){
                    if(mp.isPlaying()){
                        mp.pause();
                    }else {
                        mp.start();
                    }
                    voiceAdapter.filterList(item);
                }else {
                    CurrentPosition = item;
                    if(mp!=null){
                        mp.stop();
                        mp.release();
                        mp = null;
                    }

                    voiceAdapter.filterList(item);
                    audioPlayer(voiceModelArrayList.get(item).getVideo());
                }

            }
        });
        recVoiceProd.setAdapter(voiceAdapter);

        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }

        getProdcast(search);


        return view;

    }

    public void audioPlayer(String fileName){
//set up MediaPlayer
        mp = new MediaPlayer();

        try {
            mp.setDataSource(fileName);
            mp.prepare();
            mp.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void getProdcast(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
//getting the tag from the edittext

        GetProdcast getProdcast = new GetProdcast(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();
                voiceModelArrayList.clear();
                videosModelArrayList.clear();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        JSONObject object1 = object.getJSONObject("comocs");

                        JSONArray jsonArray = object1.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject object2 = jsonArray.getJSONObject(i);

                            ComicsModel comicsModel = new ComicsModel();
                            comicsModel.setComic_id(object2.getString("comic_id"));
                            comicsModel.setTitle(object2.getString(session.getTitle()));
                            comicsModel.setBanner(object2.getString("banner"));

                            comicsModelArrayList.add(comicsModel);
                        }

                        JSONObject object2 = object.getJSONObject("videos");

                        JSONArray jsonArray1 = object2.getJSONArray("data");

                        for (int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject1 = jsonArray1.getJSONObject(i);

                            VideosModel videosModel = new VideosModel();
                            videosModel.setTitle(jsonObject1.getString(session.getTitle()));
                            videosModel.setVideo_id(jsonObject1.getString("video_id"));
                            videosModel.setVideo(jsonObject1.getString("banner"));
                            videosModel.setCategory(jsonObject1.getString("category"));
                            videosModel.setDescription(jsonObject1.getString(session.getDescription()));
                            videosModel.setDate(jsonObject1.getString("date"));

                            videosModelArrayList.add(videosModel);
                        }

                        JSONObject object3 = object.getJSONObject("prodcasts");

                        JSONArray jsonArray2 = object3.getJSONArray("data");

                        for (int i = 0; i < jsonArray2.length(); i++){
                            JSONObject jsonObject1 = jsonArray2.getJSONObject(i);

                            VideosModel videosModel = new VideosModel();
                            videosModel.setVideo_id(jsonObject1.getString("prodcast_id"));
                            videosModel.setTitle(jsonObject1.getString(session.getTitle()));
                            videosModel.setVideo(jsonObject1.getString("audio_file"));
                            videosModel.setDescription(jsonObject1.getString(session.getDescription()));
                            videosModel.setDate(jsonObject1.getString("date"));
                            videosModel.setBanner(jsonObject1.getString("banner"));
                            videosModel.setIsPlaying(false);

                            voiceModelArrayList.add(videosModel);
                        }

                        mProadcastAdapter.notifyDataSetChanged();
                        voiceAdapter.notifyDataSetChanged();
                        videoAdapter.notifyDataSetChanged();

                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

// Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getProdcast);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mp != null){
            if(mp.isPlaying())
                mp.stop();

            mp.release();
            mp = null;
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mp != null){
            if(mp.isPlaying())
                mp.stop();

            mp.release();
            mp = null;
        }
    }

    protected void replaceFragment(@IdRes int containerViewId,
                                   @NonNull Fragment fragment,
                                   @NonNull String fragmentTag) {
        getActivity().getSupportFragmentManager()
                .beginTransaction()
                .replace(containerViewId, fragment, fragmentTag)
                .addToBackStack(null)
                .commit();
    }
    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }

}