package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.LanguageModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;

import java.util.ArrayList;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.Viewholder> {

    private final ArrayList<LanguageModel> mLanguageModels;
    private final UserSession usersession;
    private Context context;
    private final OnItemClickListener mListener;

    public LanguageAdapter(Context context, ArrayList<LanguageModel> languageModels, OnItemClickListener listener) {
        this.context = context;
        this.mLanguageModels = languageModels;
        this.mListener = listener;
        usersession  = new UserSession(context);
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_language, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.mlanguage_txt.setText(mLanguageModels.get(position).getName());

        if(mLanguageModels.get(position).getName().equals(usersession.getLanguage())){
            holder.selected_img.setVisibility(View.VISIBLE);
        }else {
            holder.selected_img.setVisibility(View.GONE);
        }

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(mLanguageModels.get(position).getName(),
                        mLanguageModels.get(position).getCode(),
                        mLanguageModels.get(position).getTitle(),
                        mLanguageModels.get(position).getDescription(),
                        mLanguageModels.get(position).getDescription2(),
                        mLanguageModels.get(position).getQuestion(),
                        mLanguageModels.get(position).getAnswer(),
                        mLanguageModels.get(position).getCategory());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mLanguageModels.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        TextView mlanguage_txt;
        ImageView selected_img;

        public Viewholder(@NonNull View itemView) {
            super(itemView);


            mlanguage_txt = itemView.findViewById(R.id.mlanguage_txt);
            selected_img = itemView.findViewById(R.id.selected_img);

        }
    }
    public interface OnItemClickListener {
        void onItemClick(String name,String code,String title, String description, String description2, String question, String answer, String category);
    }



}
