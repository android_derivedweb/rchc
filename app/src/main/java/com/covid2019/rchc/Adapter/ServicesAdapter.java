package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.ServicesModel;
import com.covid2019.rchc.R;

import java.util.ArrayList;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.MyViewHolder> {

    private final OnItemClickListener mListener;
    private Context context;
    private ArrayList<ServicesModel> servicesModelArrayList;


    public ServicesAdapter (Context context, ArrayList<ServicesModel> servicesModelArrayList, OnItemClickListener listener) {
        this.mListener = listener;
        this.context = context;
        this.servicesModelArrayList = servicesModelArrayList;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_services, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        String[] colors = {"#575757", "#30aa65", "#2e84ee", "#f2c010"};

        int color = position % colors.length;

        holder.backColor.setBackgroundColor(Color.parseColor(colors[color]));

        holder.titleSer.setText(servicesModelArrayList.get(position).getTitle());
        holder.des.setText(servicesModelArrayList.get(position).getDescription());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(servicesModelArrayList.get(position).getService_id());
            }
        });
    }

    @Override
    public int getItemCount() {
        return servicesModelArrayList.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        LinearLayout backColor;
        TextView titleSer, des;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            backColor = itemView.findViewById(R.id.backColor);
            titleSer = itemView.findViewById(R.id.titleSer);
            des = itemView.findViewById(R.id.des);

        }
    }


    public interface OnItemClickListener {
        void onItemClick(String id);
    }

}
