package com.covid2019.rchc.Api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.covid2019.rchc.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

public class GetProjectsDetails extends StringRequest {

    private Map<String, String> parameters;


    public GetProjectsDetails(String project_id, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, UserSession.BASEURL + "get-project-details?project_id=" + project_id, listener, errorListener);

        parameters = new HashMap<>();
      //  parameters.put("service_id ", OldPassword );

        Log.e("fdfd", UserSession.BASEURL + "---");
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {

    return parameters;
    }

}