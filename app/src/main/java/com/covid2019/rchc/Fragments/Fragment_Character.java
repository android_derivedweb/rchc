package com.covid2019.rchc.Fragments;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Adapter.CharacterAdapter;
import com.covid2019.rchc.Api.GetCharacters;
import com.covid2019.rchc.Model.CharacterModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Character extends Fragment {

    private RecyclerView recCharacter;
    private CharacterAdapter characterAdapter;
    private RequestQueue requestQueue;
    private ArrayList<CharacterModel> characterModelArrayList = new ArrayList<CharacterModel>();
    private String search = "";

    private UserSession session;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.activity_character, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        session = new UserSession(getContext());


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }

        recCharacter = view.findViewById(R.id.recCharacter);

        recCharacter.setLayoutManager(new LinearLayoutManager(getContext()));
        characterAdapter = new CharacterAdapter(getContext(), characterModelArrayList);
        recCharacter.setAdapter(characterAdapter);


        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }
        mSearch.setVisibility(View.VISIBLE);

        getCharacters(search);


        return view;
    }

    private void getCharacters(String search){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
        //getting the tag from the edittext

        GetCharacters getCharacters = new GetCharacters(search,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {


                        JSONObject object = jsonObject.getJSONObject("data");

                        JSONArray jsonArray = object.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject object1 = jsonArray.getJSONObject(i);

                            CharacterModel characterModel = new CharacterModel();

                            characterModel.setCharacter_id(object1.getString("character_id"));
                            characterModel.setTitle(object1.getString(session.getTitle()));

                            if (session.getLanguageCode().equals("en")) {
                                characterModel.setTitle1(object1.getString("title1"));
                                characterModel.setTitle2(object1.getString("title2"));
                            } else if (session.getLanguageCode().equals("fr")){
                                characterModel.setTitle1(object1.getString("title1_fr"));
                                characterModel.setTitle2(object1.getString("title2_fr"));
                            } else if (session.getLanguageCode().equals("rw")){
                                characterModel.setTitle1(object1.getString("title1_rw"));
                                characterModel.setTitle2(object1.getString("title2_rw"));
                            } else if (session.getLanguageCode().equals("kln")){
                                characterModel.setTitle1(object1.getString("title1_ln"));
                                characterModel.setTitle2(object1.getString("title2_ln"));
                            } else if (session.getLanguageCode().equals("sw")){
                                characterModel.setTitle1(object1.getString("title1_sw"));
                                characterModel.setTitle2(object1.getString("title2_sw"));
                            } else if (session.getLanguageCode().equals("rn")){
                                characterModel.setTitle1(object1.getString("title1_rn"));
                                characterModel.setTitle2(object1.getString("title2_rn"));
                            }


                            characterModel.setBanner(object1.getString("banner"));

                            characterModelArrayList.add(characterModel);
                        }

                        characterAdapter.notifyDataSetChanged();
                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

                //   Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getCharacters);


    }
    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    }
    @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }
}