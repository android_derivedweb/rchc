package com.covid2019.rchc.Api;

import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;
import com.covid2019.rchc.Utils.UserSession;

import java.util.HashMap;
import java.util.Map;

public class ProjectApply extends StringRequest {

    private Map<String, String> parameters;


    public ProjectApply(String project_id, String name, String email, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.POST, UserSession.BASEURL + "project-apply", listener, errorListener);
        parameters = new HashMap<>();
        parameters.put("project_id", project_id );
        parameters.put("name", name );
        parameters.put("email", email );

        Log.e("fdfd", UserSession.BASEURL + "---");
    }


    @Override
    protected Map<String, String> getParams() throws AuthFailureError {

    return parameters;
    }

}