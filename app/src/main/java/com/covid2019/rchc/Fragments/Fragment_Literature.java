package com.covid2019.rchc.Fragments;

import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.Volley;
import com.covid2019.rchc.Activity.LiteratureDetailsActivity;
import com.covid2019.rchc.Adapter.ArticalsAdapter;
import com.covid2019.rchc.Adapter.BooksAdapter;
import com.covid2019.rchc.Adapter.JournalsAdapter;
import com.covid2019.rchc.Api.GetLiterature;
import com.covid2019.rchc.Model.PopularModel;
import com.covid2019.rchc.R;
import com.covid2019.rchc.Utils.EndlessRecyclerViewScrollListener;
import com.covid2019.rchc.Utils.UserSession;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import static com.covid2019.rchc.Activity.MainActivity.mSearch;

public class Fragment_Literature extends Fragment {

    private RecyclerView recJournals, recBooks, resArticals;

    private JournalsAdapter journalsAdapter;
    private BooksAdapter booksAdapter;
    private ArticalsAdapter articalsAdapter;

    private RequestQueue requestQueue;

    private ArrayList<PopularModel> journalsModelArrayList = new ArrayList<>();
    private ArrayList<PopularModel> booksModelArrayList = new ArrayList<>();
    private ArrayList<PopularModel> articalsModelArrayList = new ArrayList<>();
    private String search = "";

    private UserSession session;


    private LinearLayoutManager linearlayoutArtical;
    private int last_sizeArtical;
    private String MpageArtical = "1";


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_literature, container, false);
        requestQueue = Volley.newRequestQueue(getContext());//Creating the RequestQueue

        session = new UserSession(getContext());

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Window window = Objects.requireNonNull(getActivity()).getWindow();
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR);// set status text dark
            ((Window) window).setStatusBarColor(getResources().getColor(R.color.white));
            ((Window) window).setNavigationBarColor(getResources().getColor(R.color.black));
        }
        recJournals = view.findViewById(R.id.recJournals);
        recBooks = view.findViewById(R.id.recBooks);
        resArticals = view.findViewById(R.id.resArticals);

        recJournals.setHasFixedSize(true);
        recBooks.setHasFixedSize(true);
        resArticals.setHasFixedSize(true);


        linearlayoutArtical = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false);
        recJournals.setLayoutManager(linearlayoutArtical);


        resArticals.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));
        recBooks.setLayoutManager(new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL, false));

        journalsAdapter = new JournalsAdapter(getContext(), journalsModelArrayList, new JournalsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String banner, String title, String des, String image, String des2) {

                Intent intent = new Intent(getActivity(), LiteratureDetailsActivity.class);
                intent.putExtra("identLite", "journals");
                intent.putExtra("bannerLite", banner);
                intent.putExtra("titleLite", title);
                intent.putExtra("desLite", des);
                intent.putExtra("imageLite", image);
                intent.putExtra("des2Lite", des2);
                startActivity(intent);
            }
        });
        recJournals.setAdapter(journalsAdapter);

        articalsAdapter = new ArticalsAdapter(getContext(), articalsModelArrayList, new ArticalsAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String banner, String title, String des, String image, String des2) {
                Intent intent = new Intent(getActivity(), LiteratureDetailsActivity.class);
                intent.putExtra("identLite", "articals");
                intent.putExtra("bannerLite", banner);
                intent.putExtra("titleLite", title);
                intent.putExtra("desLite", des);
                intent.putExtra("imageLite", image);
                intent.putExtra("des2Lite", des2);
                startActivity(intent);
            }
        });
        resArticals.setAdapter(articalsAdapter);

        resArticals.addOnScrollListener(new EndlessRecyclerViewScrollListener(linearlayoutArtical) {
            @Override
            public void onLoadMore(int page, int totalItemsCount) {
                Log.e("PageStatus",page + "  " + last_sizeArtical);
                if (page!=last_sizeArtical){
                    MpageArtical = String.valueOf(page+1);

                    getLiterature("", MpageArtical);

                }
            }
        });


        booksAdapter = new BooksAdapter(getContext(), booksModelArrayList, new BooksAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String banner, String title, String des, String image, String des2) {
                Intent intent = new Intent(getActivity(), LiteratureDetailsActivity.class);
                intent.putExtra("identLite", "books");
                intent.putExtra("bannerLite", banner);
                intent.putExtra("titleLite", title);
                intent.putExtra("desLite", des);
                intent.putExtra("imageLite", image);
                intent.putExtra("des2Lite", des2);
                startActivity(intent);
            }
        });
        recBooks.setAdapter(booksAdapter);

        Bundle bundle = getArguments();
        try {
            search = bundle.getString("search");
            Log.e("search", search);
        }catch (Exception e){

        }
        mSearch.setVisibility(View.VISIBLE);
        getLiterature(search, "1");

        return view;
    }


    private void getLiterature(String search, String page){
        final KProgressHUD progressDialog = KProgressHUD.create(getContext())
                .setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Please wait")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .setDimAmount(0.5f)
                .show();
//getting the tag from the edittext

        GetLiterature getLiterature = new GetLiterature(search, page,new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.e("ssdffdf", response + "---");


                progressDialog.dismiss();

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    Log.e("ResponseServ", jsonObject.toString());

                    if (jsonObject.getString("ResponseCode").equals("200")) {

                        JSONObject jsonObject1 = jsonObject.getJSONObject("journals");
                        JSONArray jsonArray = jsonObject1.getJSONArray("data");

                        for (int i = 0; i < jsonArray.length(); i++){
                            JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                            PopularModel popularModel = new PopularModel();
                            popularModel.setService_id(jsonObject2.getString("journal_id"));
                            popularModel.setTitle(jsonObject2.getString(session.getTitle()));
                            popularModel.setBanner(jsonObject2.getString("banner"));
                            popularModel.setDescription(jsonObject2.getString(session.getDescription()));
                            popularModel.setDescription2(jsonObject2.getString(session.getDescription2()));
                            popularModel.setBanner_thumb(jsonObject2.getString("banner_thumb"));
                            popularModel.setImage_thumb(jsonObject2.getString("image_thumb"));

                            journalsModelArrayList.add(popularModel);
                        }

                        JSONObject jsonObject2 = jsonObject.getJSONObject("books");
                        JSONArray jsonArray1 = jsonObject2.getJSONArray("data");

                        for (int i = 0; i < jsonArray1.length(); i++){
                            JSONObject jsonObject3 = jsonArray1.getJSONObject(i);

                            PopularModel popularModel = new PopularModel();
                            popularModel.setService_id(jsonObject3.getString("book_id"));
                            popularModel.setTitle(jsonObject3.getString(session.getTitle()));
                            popularModel.setBanner(jsonObject3.getString("banner"));
                            popularModel.setDescription(jsonObject3.getString(session.getDescription()));
                            popularModel.setDescription2(jsonObject3.getString(session.getDescription2()));
                            popularModel.setBanner_thumb(jsonObject3.getString("banner_thumb"));
                            popularModel.setImage_thumb(jsonObject3.getString("image_thumb"));

                            booksModelArrayList.add(popularModel);
                        }

                        JSONObject jsonObject5 = jsonObject.getJSONObject("articals");
                        last_sizeArtical = jsonObject5.getInt("last_page");

                        JSONArray jsonArray4 = jsonObject5.getJSONArray("data");

                        for (int i = 0; i < jsonArray4.length(); i++){
                            JSONObject jsonObject7 = jsonArray4.getJSONObject(i);

                            PopularModel popularModel = new PopularModel();
                            popularModel.setService_id(jsonObject7.getString("artical_id"));
                            popularModel.setTitle(jsonObject7.getString(session.getTitle()));
                            popularModel.setBanner(jsonObject7.getString("banner"));
                            popularModel.setDescription(jsonObject7.getString(session.getDescription()));
                            popularModel.setDescription2(jsonObject7.getString(session.getDescription2()));
                            popularModel.setBanner_thumb(jsonObject7.getString("banner_thumb"));
                            popularModel.setImage_thumb(jsonObject7.getString("image_thumb"));

                            articalsModelArrayList.add(popularModel);
                        }



                        journalsAdapter.notifyDataSetChanged();
                        booksAdapter.notifyDataSetChanged();
                        articalsAdapter.notifyDataSetChanged();

                    }

                } catch (Exception e) {
                    Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();

                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();

                Log.e("onErrorResponse", error.getMessage());

// Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        requestQueue.add(getLiterature);


    }

    public void setLocale(String lang) {

        Locale myLocale = new Locale(lang);
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = myLocale;
        res.updateConfiguration(conf, dm);
    } @Override
    public void onResume() {
        super.onResume();
        UserSession userSession = new UserSession(getActivity());
        setLocale(userSession.getLanguageCode());
    }

}