package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.covid2019.rchc.Model.IssueModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class IssueViewAdapter extends PagerAdapter {

    private Context mContext;
    private ArrayList<IssueModel> issueModelArrayList;

    public IssueViewAdapter(Context context , ArrayList<IssueModel> issueModelArrayList) {
        this.mContext = context;
        this.issueModelArrayList = issueModelArrayList;
    }
  
    @Override  
    public boolean isViewFromObject(View view, Object object) {
        return view == ((View) object);
    }  

    @Override  
    public Object instantiateItem(ViewGroup container, int position) {

        View myImageLayout = LayoutInflater.from(mContext).inflate(R.layout.adapter_issues, container, false);

        ImageView imageIsuue = myImageLayout.findViewById(R.id.imageIsuue);
        TextView issue_title = myImageLayout.findViewById(R.id.issue_title);
        TextView issueDes = myImageLayout.findViewById(R.id.issueDes);

        issue_title.setText(issueModelArrayList.get(position).getTitle());
        issueDes.setText(issueModelArrayList.get(position).getDescription());
        Picasso.get().load(issueModelArrayList.get(position).getBanner()).into(imageIsuue);

        Log.e("sizzzz", issueModelArrayList.size() + "--");

        ((ViewPager) container).addView(myImageLayout, 0);

        return myImageLayout;

    }  
  
    @Override  
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }
  
    @Override  
    public int getCount() {  
        return issueModelArrayList.size();
    }

}