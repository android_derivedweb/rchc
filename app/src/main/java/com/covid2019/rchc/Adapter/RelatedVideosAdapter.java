package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.covid2019.rchc.Model.VideoModel;
import com.covid2019.rchc.R;

import java.util.ArrayList;

public class RelatedVideosAdapter extends RecyclerView.Adapter<RelatedVideosAdapter.Viewholder> {

    private Context context;
    private ArrayList<VideoModel> videoModelArrayList;
    private final OnItemClickListener mListener;

    public RelatedVideosAdapter(Context context, ArrayList<VideoModel> videoModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.videoModelArrayList = videoModelArrayList;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public Viewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_related_videos, parent, false);
        return new Viewholder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Viewholder holder, int position) {

        holder.rldVideoTitle.setText(videoModelArrayList.get(position).getTitle());
        holder.rltdVideoCategory.setText(videoModelArrayList.get(position).getCategory());

        Glide.with(context).load(videoModelArrayList.get(position).getVideo()).into(holder.rltdVideos);
       // Picasso.get().load(videoModelArrayList.get(position).getVideo()).into(holder.rltdVideos);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(videoModelArrayList.get(position).getVideo_id());
            }
        });

    }

    @Override
    public int getItemCount() {
        return videoModelArrayList.size();
    }

    public class Viewholder extends RecyclerView.ViewHolder {

        ImageView rltdVideos;
        TextView rldVideoTitle, rltdVideoCategory;

        public Viewholder(@NonNull View itemView) {
            super(itemView);

            rltdVideoCategory = itemView.findViewById(R.id.rltdVideoCategory);
            rltdVideos = itemView.findViewById(R.id.rltdVideos);
            rldVideoTitle = itemView.findViewById(R.id.rldVideoTitle);

        }
    }

    public interface OnItemClickListener {
        void onItemClick(String id);
    }

}
