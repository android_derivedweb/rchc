package com.covid2019.rchc.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.covid2019.rchc.Model.PopularModel;
import com.covid2019.rchc.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class PopularAdapter extends RecyclerView.Adapter<PopularAdapter.MyViewHolder> {

    private Context context;
    private ArrayList<PopularModel> popularModelArrayList;
    private final OnItemClickListener mListener;


    public PopularAdapter(Context context, ArrayList<PopularModel> popularModelArrayList, OnItemClickListener listener) {
        this.context = context;
        this.popularModelArrayList = popularModelArrayList;
        this.mListener = listener;

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView homeImage;
        TextView titleHome, desHome, dateHome;

        public MyViewHolder( View itemView) {
            super(itemView);

            homeImage = itemView.findViewById(R.id.homeImage);
            titleHome = itemView.findViewById(R.id.titleHome);
            desHome = itemView.findViewById(R.id.desHome);
            dateHome = itemView.findViewById(R.id.dateHome);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View view = layoutInflater.inflate(R.layout.adapter_home, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder( MyViewHolder holder, int position) {
        holder.titleHome.setText(popularModelArrayList.get(position).getTitle());
        holder.desHome.setText(popularModelArrayList.get(position).getDescription());
        holder.dateHome.setText(popularModelArrayList.get(position).getDate());
        Picasso.get().load(popularModelArrayList.get(position).getBanner()).into(holder.homeImage);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListener.onItemClick(popularModelArrayList.get(position).getService_id());
            }
        });

    }

    @Override
    public int getItemCount() {
        return popularModelArrayList.size();
    }


    public interface OnItemClickListener {
        void onItemClick(String id);
    }

}
